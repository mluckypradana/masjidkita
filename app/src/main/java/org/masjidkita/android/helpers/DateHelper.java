package org.masjidkita.android.helpers;

import android.content.Context;
import android.content.SharedPreferences;

import org.masjidkita.android.Config;

/**
 * Helper class for SharedPreferences
 */
public final class DateHelper {
    public static String parseDate(String date) {
        String[] dateArray = date.split("-");
        int year = Integer.parseInt(dateArray[0]);
        int month = Integer.parseInt(dateArray[1]);
        int day = Integer.parseInt(dateArray[2]);
        String text = day + " ";
        switch (month) {
            case 1:
                text += "Januari";
                break;
            case 2:
                text += "Februari";
                break;
            case 3:
                text += "Maret";
                break;
            case 4:
                text += "April";
                break;
            case 5:
                text += "Mei";
                break;
            case 6:
                text += "Juni";
                break;
            case 7:
                text += "Juli";
                break;
            case 8:
                text += "Agustus";
                break;
            case 9:
                text += "September";
                break;
            case 10:
                text += "Oktober";
                break;
            case 11:
                text += "November";
                break;
            case 12:
                text += "Desember";
                break;
            default:
                break;
        }
        text += " " + year;
        return text;
    }
}