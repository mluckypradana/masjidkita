package org.masjidkita.android.helpers;

import android.content.Context;
import android.content.SharedPreferences;

import org.masjidkita.android.Config;

/**
 * Helper class for SharedPreferences
 */
public final class SessionHelper {
    public static final String USER_ID = "USER_ID";
    public static final String TOKEN = "TOKEN";
    public static final String LATITUDE = "LATITUDE";
    public static final String LONGITUDE = "LONGITUDE";
    public static final String ABOUT_MTW = "ABOUT_MTW";
    public static final String NAME = "NAME";
    public static final String EMAIL = "EMAIL";

    // Shared Preferences
    SharedPreferences mPreferences;
    SharedPreferences.Editor mEditor;
    Context _context;

    // Constructor
    public SessionHelper(Context context) {
        this._context = context;
        mPreferences = _context.getSharedPreferences(Config.Pref.NAME, Config.Pref.PRIVATE_MODE);
        mEditor = mPreferences.edit();
    }

    /*
    * Set to preferences
    */
    public void put(String label, String value) {
        mEditor.putString(label, value).commit();
    }

    /*
    * Set to preferences for integer
    */
    public void put(String label, Integer value) {
        mEditor.putInt(label, value).commit();
    }

    /*
    * Set to preferences for boolean
    */
    public void put(String label, Boolean value) {
        mEditor.putBoolean(label, value).commit();
    }

    /**
     * Get value of shared preference
     */
    public String getString(String label) {
        return mPreferences.getString(label, null);
    }

    /**
     * Get value of shared preference
     */
    public int getInt(String label) {
        return mPreferences.getInt(label, 0);
    }

    /**
     * Get value of shared preference
     *
     * @return
     */
    public boolean getBoolean(String label) {
        return mPreferences.getBoolean(label, false);
    }
}