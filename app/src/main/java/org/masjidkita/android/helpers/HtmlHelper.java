package org.masjidkita.android.helpers;

import android.text.Html;
import android.text.Spanned;

/**
 * Created by MuhammadLucky on 21/09/2016.
 */
public class HtmlHelper {
    public static Spanned fromHtml(String text) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N)
            result = Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY);
        else
            result = Html.fromHtml(text);

        return result;
    }
}
