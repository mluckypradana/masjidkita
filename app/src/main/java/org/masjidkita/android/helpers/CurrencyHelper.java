package org.masjidkita.android.helpers;

import java.text.DecimalFormat;

/**
 * Helper class for SharedPreferences
 */
public final class CurrencyHelper {
    public static String parseCurrency(long currency){
        return new DecimalFormat("#,###.##").format(currency);
    }
}