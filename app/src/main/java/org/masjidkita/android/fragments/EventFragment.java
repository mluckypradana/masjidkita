package org.masjidkita.android.fragments;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ViewAnimator;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

import org.masjidkita.android.Config;
import org.masjidkita.android.R;
import org.masjidkita.android.activities.DetailActivity;
import org.masjidkita.android.activities.MainActivity;
import org.masjidkita.android.adapters.EventAdapter;
import org.masjidkita.android.adapters.ScrollListener;
import org.masjidkita.android.database.models.Event;
import org.masjidkita.android.database.models.Feed;
import org.masjidkita.android.helpers.SessionHelper;
import org.masjidkita.android.network.response.EventsResponse;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link CoreFragment} subclass.
 */
public class EventFragment extends CoreFragment implements
        EventAdapter.OnClickListener,
        ScrollListener {

    private static int VIEW_FOLLOWED = 0;
    private static int VIEW_ALL = 1;

    @Bind(R.id.rv_data)
    RecyclerView rvData;
    @Bind(R.id.va_main)
    ViewAnimator vaMain;
    @Bind(R.id.sr_main)
    SwipeRefreshLayout srMain;
    @Bind(R.id.sp_sort)
    Spinner spSort;

    private List<Event> events = new ArrayList<>();
    private EventAdapter mAdapter;
    private Dao<Event, Integer> EventDao;
    private boolean loading;
    private boolean endOfData;
    private boolean fromNoConnection;
    private int currentView = VIEW_FOLLOWED;
    private boolean mine;

    public EventFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_feed_with_sort, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupDao();
        if (mine) {
            ((MainActivity) getActivity()).getSupportActionBar().setTitle(R.string.nav_ustadz_schedule);
            setupSpinnerAdapter();
        }
        setupAdapter();
        loadData();
    }


    @OnClick(R.id.ll_failed_fetch)
    void tryFetch() {
        loadServerData(0);
    }


    private void setupDao() {
        try {
            EventDao = getDatabase().getDao(Event.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Load data from server / local
     */
    private void loadData() {
        loadLocalData();
        if (isNetworkConnected())
            loadServerData(0);
    }

    /**
     * Load data from  local
     */
    private void loadLocalData() {
        try {
            showLoading();
            events.clear();

            QueryBuilder<Event, Integer> builder = EventDao.queryBuilder();
            //If from followed mosque
            if (mine && currentView == VIEW_FOLLOWED)
                builder.where().eq(Event.FROM_FOLLOWED_MOSQUE, true);
            builder.limit((long) Config.Limit.DATA);
            builder.orderBy(Event.DATE, false);

            events.addAll(builder.query());
            mAdapter.notifyDataSetChanged();
            showCompletedData();
        } catch (SQLException e) {
            showError();
            e.printStackTrace();
        }
    }

    /**
     * Load data from server if connected, if offset 0 = load first data
     */
    private void loadServerData(final int offset) {
        loading = true;

        int userId = getSession().getInt(SessionHelper.USER_ID);
        String token = getSession().getString(SessionHelper.TOKEN);

        showLoading();

        //If from Event menu
        if (mine) {
            Call<EventsResponse> call = getService().getMyEvents(userId, token, offset, Config.Limit.DATA, currentView);
            call.enqueue(new Callback<EventsResponse>() {
                @Override
                public void onResponse(Call<EventsResponse> call, Response<EventsResponse> response) {
                    onLoadCompleted(response, offset);
                }

                @Override
                public void onFailure(Call<EventsResponse> call, Throwable t) {
                    showError();
                }
            });
        }
        //If from Event tab
        else {
            Call<EventsResponse> call = getService().getEvents(userId, token, offset, Config.Limit.DATA);
            call.enqueue(new Callback<EventsResponse>() {
                @Override
                public void onResponse(Call<EventsResponse> call, Response<EventsResponse> response) {
                    onLoadCompleted(response, offset);
                }

                @Override
                public void onFailure(Call<EventsResponse> call, Throwable t) {
                    showError();
                }
            });
        }
    }

    /**
     * On success for every methodd
     */
    private void onLoadCompleted(Response<EventsResponse> response, int offset) {
        loading = false;

        if (response.code() == 200) {
            EventsResponse EventsResponse = response.body();

            //Clear data on first fetch
            if (offset == 0) {
                endOfData = false;

                events.clear();
//                try {
//                    if (getActivity() != null)
//                        TableUtils.clearTable(getDatabase().getConnectionSource(), Feed.class);
//                } catch (SQLException e) {
//                    e.printStackTrace();
//                }
            }

            //Save to database
            try {
                if (EventsResponse.getData() != null)
                    for (Event Event : EventsResponse.getData())
                        EventDao.createOrUpdate(Event);
            } catch (SQLException e) {
                e.printStackTrace();
            }

            if (EventsResponse.getData().size() > 0)
                events.addAll(EventsResponse.getData());
            mAdapter.notifyDataSetChanged();

            //Don't load
            if (EventsResponse.getData().size() == 0 && offset > 0)
                endOfData = true;

        } else
            showToast(R.string.message_failed_fetch);
        showCompletedData();
    }

    /**
     * Show load progress if datalist equals 0
     */
    private void showLoading() {
        if (events.size() == 0)
            vaMain.setDisplayedChild(LAYOUT_PROGRESS);
    }

    /**
     * Set view animator visibility if necessary
     */
    private void showCompletedData() {
        if (srMain == null)
            return;
        srMain.setRefreshing(false);
        vaMain.setDisplayedChild(events.size() > 0 ? LAYOUT_MAIN : LAYOUT_NO_DATA);
    }

    /**
     * Show error page
     */
    private void showError() {
        if (srMain == null)
            return;
        srMain.setRefreshing(false);
        if (events.size() == 0)
            vaMain.setDisplayedChild(LAYOUT_FAILED);
        else
            showToast(R.string.message_failed_fetch);
    }

    /**
     * Setup spinner toolbar for sort function
     */
    private void setupSpinnerAdapter() {
        final String[] items = getResources().getStringArray(R.array.spinner_feed_sort);
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.item_spinner_toolbar, R.id.sp_text, items) {
//            public View getView(int position, View convertView, ViewGroup parent) {
//                View v = convertView;
//                if (v == null)
//                    v = LayoutInflater.from(getContext()).inflate(R.layout.item_spinner_toolbar, parent, false);
//                TextView tv = (TextView) v.findViewById(R.id.sp_text);
//                tv.setText(items[position]);
//                return v;
//            }
//        };
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.item_list_spinner, items);
        spSort.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
//                    ((TextView) view).setTextAppearance(R.style.Spinner_Toolbar);
//                else
//                    ((TextView) view).setTextAppearance(getContext(), R.style.Spinner_Toolbar);
                ((TextView) view).setTextColor(ContextCompat.getColor(getContext(), R.color.white));
                view.setBackgroundResource(R.drawable.sp_toolbar_selected);
                sortData(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spSort.setAdapter(adapter);
        spSort.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        spSort.setVisibility(View.VISIBLE);
        spSort.setSelection(currentView);
    }

    /**
     * Sort list data by position
     *
     * @param sort As sort spinner index
     */
    private void sortData(int sort) {
        currentView = sort;
        loadLocalData();
        if (isNetworkConnected())
            loadServerData(0);
    }

    /**
     * Populate recyclerview from adapter
     */
    private void setupAdapter() {
        srMain.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadServerData(0);
            }
        });

        mAdapter = new EventAdapter(this, events);
        rvData.setAdapter(mAdapter);
        rvData.addOnScrollListener(mAdapter.getScrollListener(this, events));
//        rvData.setOverScrollMode(View.OVER_SCROLL_ALWAYS);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvData.setLayoutManager(layoutManager);
    }

    @Override
    public void onMosqueClicked(int position) {
        //TODO Show mosque detail
    }

    @Override
    public void onItemClicked(int position) {
        Intent intent = new Intent(getActivity(), DetailActivity.class);
        intent.putExtra(Config.Extra.TYPE, Feed.TYPE_EVENT);
        intent.putExtra(Config.Extra.ID, events.get(position).getId());
        startActivity(intent);
    }

    @Override
    public void onScrolledTop() {
    }

    /**
     * Load more if connected
     */
    @Override
    public void onScrolledBottom() {
        if (isNetworkConnected() && !loading && !endOfData)
            loadServerData(events.size());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    /**
     * If mine = true, this fragment will get my Event from server
     *
     * @param mine
     */
    public void setMine(boolean mine) {
        this.mine = mine;
    }
}