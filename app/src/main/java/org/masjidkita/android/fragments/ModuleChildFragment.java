package org.masjidkita.android.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.ViewAnimator;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.table.TableUtils;

import org.masjidkita.android.Config;
import org.masjidkita.android.R;
import org.masjidkita.android.activities.DetailActivity;
import org.masjidkita.android.adapters.ModuleAdapter;
import org.masjidkita.android.adapters.ScrollListener;
import org.masjidkita.android.database.models.Feed;
import org.masjidkita.android.database.models.Module;
import org.masjidkita.android.helpers.SessionHelper;
import org.masjidkita.android.network.response.ModulesResponse;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link CoreFragment} subclass.
 */
public class ModuleChildFragment extends CoreFragment implements
        ModuleAdapter.OnClickListener,
        ScrollListener {

    @Bind(R.id.rv_data)
    RecyclerView rvData;
    @Bind(R.id.va_main)
    ViewAnimator vaMain;
    @Bind(R.id.sr_main)
    SwipeRefreshLayout srMain;

    private List<Module> modules = new ArrayList<>();
    private ModuleAdapter mAdapter;
    private Dao<Module, Integer> moduleDao;
    private boolean loading;
    private boolean endOfData;
    private boolean fromNoConnection;
    private int type = Module.TYPE_FEED;

    public ModuleChildFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_feed, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupDao();
        setupAdapter();
        loadData();
    }


    @OnClick(R.id.ll_failed_fetch)
    void tryFetch() {
        loadServerData(0);
    }


    private void setupDao() {
        try {
            moduleDao = getDatabase().getDao(Module.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Load data from server / local
     */
    private void loadData() {
        loadLocalData();
        if (isNetworkConnected())
            loadServerData(0);
    }

    /**
     * Load data from  local
     */
    private void loadLocalData() {
        try {
            showLoading();
            modules.clear();

            QueryBuilder<Module, Integer> builder = moduleDao.queryBuilder();
            //If from followed mosque
            builder.where().eq(Module.TYPE, type);
            builder.limit((long) Config.Limit.DATA);

            modules.addAll(builder.query());
            mAdapter.notifyDataSetChanged();
            showCompletedData();
        } catch (SQLException e) {
            showError();
            e.printStackTrace();
        }
    }

    /**
     * Load data from server if connected, if offset 0 = load first data
     */
    private void loadServerData(final int offset) {
        loading = true;

        int userId = getSession().getInt(SessionHelper.USER_ID);
        String token = getSession().getString(SessionHelper.TOKEN);

        showLoading();

        //If from Module menu
        if (type == Module.TYPE_FEED) {
            Call<ModulesResponse> call = getService().getModules(userId, token, offset, Config.Limit.DATA, 0);
            call.enqueue(new Callback<ModulesResponse>() {
                @Override
                public void onResponse(Call<ModulesResponse> call, Response<ModulesResponse> response) {
                    onLoadCompleted(response, offset);
                }

                @Override
                public void onFailure(Call<ModulesResponse> call, Throwable t) {
                    showError();
                }
            });
        }
        //If from Module tab
        else {
            String typeRequest = Module.TYPE_REQUEST_DOWNLOAD;
            if (type == Module.TYPE_UPLOAD)
                typeRequest = Module.TYPE_REQUEST_UPLOAD;
            Call<ModulesResponse> call = getService().getMyModules(userId, token, offset, Config.Limit.DATA, typeRequest);
            call.enqueue(new Callback<ModulesResponse>() {
                @Override
                public void onResponse(Call<ModulesResponse> call, Response<ModulesResponse> response) {
                    onLoadCompleted(response, offset);
                }

                @Override
                public void onFailure(Call<ModulesResponse> call, Throwable t) {
                    showError();
                }
            });
        }
    }

    /**
     * On success for every methodd
     */
    private void onLoadCompleted(Response<ModulesResponse> response, int offset) {
        loading = false;

        if (response.code() == 200) {
            ModulesResponse ModulesResponse = response.body();

            //Clear data on first fetch
            if (offset == 0) {
                endOfData = false;

                modules.clear();
//                try {
//                    if (getActivity() != null)
//                        TableUtils.clearTable(getDatabase().getConnectionSource(), Feed.class);
//                } catch (SQLException e) {
//                    e.printStackTrace();
//                }
            }

            //Save to database
            try {
                if (ModulesResponse.getData() != null)
                    for (Module module : ModulesResponse.getData()) {
                        module.setType(type);
                        moduleDao.createOrUpdate(module);
                    }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            if (ModulesResponse.getData().size() > 0)
                modules.addAll(ModulesResponse.getData());
            mAdapter.notifyDataSetChanged();

            //Don't load
            if (ModulesResponse.getData().size() == 0 && offset > 0)
                endOfData = true;

        } else
            showToast(R.string.message_failed_fetch);
        showCompletedData();
    }

    /**
     * Show load progress if datalist equals 0
     */
    private void showLoading() {
        if (modules.size() == 0)
            vaMain.setDisplayedChild(LAYOUT_PROGRESS);
    }

    /**
     * Set view animator visibility if necessary
     */
    private void showCompletedData() {
        if (srMain == null)
            return;
        srMain.setRefreshing(false);
        vaMain.setDisplayedChild(modules.size() > 0 ? LAYOUT_MAIN : LAYOUT_NO_DATA);
    }

    /**
     * Show error page
     */
    private void showError() {
        if (srMain == null)
            return;
        srMain.setRefreshing(false);
        if (modules.size() == 0)
            vaMain.setDisplayedChild(LAYOUT_FAILED);
        else
            showToast(R.string.message_failed_fetch);
    }

    /**
     * Populate recyclerview from adapter
     */
    private void setupAdapter() {
        srMain.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadServerData(0);
            }
        });

        mAdapter = new ModuleAdapter(this, modules);
        rvData.setAdapter(mAdapter);
        rvData.addOnScrollListener(mAdapter.getScrollListener(this, modules));
//        rvData.setOverScrollMode(View.OVER_SCROLL_ALWAYS);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvData.setLayoutManager(layoutManager);
    }

    @Override
    public void onMosqueClicked(int position) {
        //TODO Show mosque detail
    }

    @Override
    public void onItemClicked(int position) {
        Intent intent = new Intent(getActivity(), DetailActivity.class);
        intent.putExtra(Config.Extra.TYPE, Feed.TYPE_MODULE);
        intent.putExtra(Config.Extra.ID, modules.get(position).getId());
        startActivity(intent);
    }

    @Override
    public void onScrolledTop() {
    }

    /**
     * Load more if connected
     */
    @Override
    public void onScrolledBottom() {
        if (isNetworkConnected() && !loading && !endOfData)
            loadServerData(modules.size());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    /**
     * If type = true, this fragment will get my Module from server
     */
    public void setType(int type) {
        this.type = type;
    }
}