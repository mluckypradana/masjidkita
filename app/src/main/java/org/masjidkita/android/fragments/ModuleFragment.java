package org.masjidkita.android.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.masjidkita.android.R;
import org.masjidkita.android.activities.MainActivity;
import org.masjidkita.android.adapters.ViewPagerAdapter;
import org.masjidkita.android.database.models.Module;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class ModuleFragment extends CoreFragment {


    @Bind(R.id.tl_main)
    TabLayout tlMain;
    @Bind(R.id.vp_main)
    ViewPager vpMain;
    private ViewPagerAdapter adapter;

    public ModuleFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_module, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((MainActivity)getActivity()).getSupportActionBar().setTitle(R.string.nav_my_module);
        setupViewPager(vpMain);
    }

    /**
     * Populate fragments inside viewpager
     *
     * @param viewPager
     */
    private void setupViewPager(ViewPager viewPager) {
        //Prevent first fragment not showing up (Don't reinitiate adpater)
//        if(adapter!=null)
//            return;

        adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        tlMain.setupWithViewPager(viewPager);

        ModuleChildFragment downloadFragment = new ModuleChildFragment();
        downloadFragment.setType(Module.TYPE_DOWNLOAD);
        ModuleChildFragment uploadFragment = new ModuleChildFragment();
        uploadFragment.setType(Module.TYPE_UPLOAD);
        ModuleChildFragment moduleChildFragment = new ModuleChildFragment();
        adapter.addFragment(downloadFragment, getString(R.string.title_tab_download));
        adapter.addFragment(uploadFragment, getString(R.string.title_tab_upload));
        viewPager.setAdapter(adapter);
        viewPager.getAdapter().notifyDataSetChanged();
        viewPager.destroyDrawingCache();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
