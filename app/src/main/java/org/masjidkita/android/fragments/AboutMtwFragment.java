package org.masjidkita.android.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.masjidkita.android.R;
import org.masjidkita.android.activities.MainActivity;
import org.masjidkita.android.adapters.ScrollListener;
import org.masjidkita.android.helpers.HtmlHelper;
import org.masjidkita.android.helpers.SessionHelper;
import org.masjidkita.android.network.ApiService;

import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * A simple {@link CoreFragment} subclass.
 */
public class AboutMtwFragment extends CoreFragment implements
        ScrollListener {

    @Bind(R.id.tv_content)
    TextView tvContent;
    private boolean loading;
    private ApiService service;

    public AboutMtwFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_about_mtw, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(R.string.nav_about_mtw);

        // Nothing to add in your builder
        Retrofit retrofit;
        retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .build();
        service = retrofit.create(ApiService.class);

//        setupDao();
//        setupSpinnerAdapter();
//        setupAdapter();
        loadData();
    }


    /**
     * Load data from server / local
     */
    private void loadData() {
        loadLocalData();
        if (isNetworkConnected())
            loadServerData(0);
    }

    /**
     * Load mosque data from local with realms
     */
    private void loadLocalData() {
        String content = getSession().getString(SessionHelper.ABOUT_MTW);
        if (content == null)
            content = "";
        tvContent.setText(HtmlHelper.fromHtml(content));
    }

    /**
     * Load mosque data from API with retrofit
     */
    private void loadServerData(final int offset) {
        loading = true;

//        int userId = getSession().getInt(SessionHelper.USER_ID);
//        String token = getSession().getString(SessionHelper.TOKEN);

//        showLoading();
        Call<ResponseBody> call = service.getMtwContent();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    //Convert reponse to string
                    try {
                        tvContent.setText(HtmlHelper.fromHtml(response.body().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else
                    showToast(R.string.error_fetch_data);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                showToast(R.string.error_fetch_data);
            }
        });
    }


    @Override
    public void onScrolledTop() {

    }

    /**
     * Load more if connected
     */
    @Override
    public void onScrolledBottom() {
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
