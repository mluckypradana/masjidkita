package org.masjidkita.android.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ViewAnimator;

import com.j256.ormlite.dao.Dao;

import org.masjidkita.android.Config;
import org.masjidkita.android.R;
import org.masjidkita.android.activities.DetailActivity;
import org.masjidkita.android.activities.MainActivity;
import org.masjidkita.android.adapters.MosqueAdapter;
import org.masjidkita.android.adapters.ScrollListener;
import org.masjidkita.android.database.models.Mosque;
import org.masjidkita.android.helpers.SessionHelper;
import org.masjidkita.android.network.response.BaseResponse;
import org.masjidkita.android.network.response.MosquesResponse;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link CoreFragment} subclass.
 */
public class AllMosqueFragment extends CoreFragment implements
        ScrollListener,
        MosqueAdapter.OnClickListener {

    @Bind(R.id.rv_data)
    RecyclerView rvData;
    @Bind(R.id.va_main)
    ViewAnimator vaMain;
    @Bind(R.id.sr_main)
    SwipeRefreshLayout srMain;

    private List<Mosque> mosques = new ArrayList<>();
    private MosqueAdapter adapter;
    private Dao<Mosque, Integer> mainDao;
    private boolean loading;
    private boolean endOfData;
    private boolean fromNoConnection;

    public AllMosqueFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        EventBus.getDefault().registerWithSocialAccount(this);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_feed, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(R.string.nav_all_mosque);

        setupDao();
        setupAdapter();
        loadData();
    }


    @OnClick(R.id.ll_failed_fetch)
    void tryFetch() {
        loadServerData(0);
    }


    private void setupDao() {
        try {
            mainDao = getDatabase().getDao(Mosque.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Load data from server / local
     */
    private void loadData() {
        loadLocalData();
        if (isNetworkConnected())
            loadServerData(0);
    }

    /**
     * Load data from  local
     */
    private void loadLocalData() {
        try {
            showLoading();
            mosques.clear();
            mosques.addAll(mainDao.queryBuilder()
                    .limit((long) Config.Limit.DATA)
                    .orderBy(Mosque.FOLLOWER, false)
                    .query());
            adapter.notifyDataSetChanged();
            showCompletedData();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Load data from server if connected, if offset 0 = load first data
     */
    private void loadServerData(final int offset) {
        loading = true;

        int userId = getSession().getInt(SessionHelper.USER_ID);
        String token = getSession().getString(SessionHelper.TOKEN);

        showLoading();
        Call<MosquesResponse> call = getService().getMosques(userId, token, offset, Config.Limit.DATA);
        call.enqueue(new Callback<MosquesResponse>() {
            @Override
            public void onResponse(Call<MosquesResponse> call, Response<MosquesResponse> response) {
                loading = false;

                if (response.code() == 200) {
                    MosquesResponse dataResponse = response.body();

                    //Clear data on first fetch
                    if (offset == 0) {
                        endOfData = false;

                        mosques.clear();
//                        try {
//                            TableUtils.clearTable(getDatabase().getConnectionSource(), Feed.class);
//                        } catch (SQLException e) {
//                            e.printStackTrace();
//                        }
                    }

                    //Save to database
                    try {
                        if (dataResponse.getData() != null)
                            for (Mosque data : dataResponse.getData())
                                mainDao.createOrUpdate(data);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                    mosques.addAll(dataResponse.getData());
                    adapter.notifyDataSetChanged();

                    //Don't load
                    if (dataResponse.getData().size() == 0 && offset > 0)
                        endOfData = true;

                } else
                    showToast(R.string.message_failed_fetch);
                showCompletedData();
            }

            @Override
            public void onFailure(Call<MosquesResponse> call, Throwable t) {
                showError();
            }
        });
    }

    /**
     * Show load progress if datalist equals 0
     */
    private void showLoading() {
        if (mosques.size() == 0)
            vaMain.setDisplayedChild(LAYOUT_PROGRESS);
    }

    /**
     * Set view animator visibility if necessary
     */
    private void showCompletedData() {
        if (srMain == null)
            return;
        srMain.setRefreshing(false);
        vaMain.setDisplayedChild(mosques.size() > 0 ? LAYOUT_MAIN : LAYOUT_NO_DATA);
    }

    /**
     * Show error page
     */
    private void showError() {
        if (srMain == null)
            return;
        srMain.setRefreshing(false);
        if (mosques.size() == 0)
            vaMain.setDisplayedChild(LAYOUT_FAILED);
        else
            showToast(R.string.message_failed_fetch);
    }

    /**
     * Populate recyclerview from adapter
     */
    private void setupAdapter() {
        srMain.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadServerData(0);
            }
        });

        adapter = new MosqueAdapter(this, mosques);
        rvData.setAdapter(adapter);
        rvData.addOnScrollListener(adapter.getScrollListener(this, mosques));

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvData.setLayoutManager(layoutManager);
    }

    @Override
    public void onFollowClicked(int position) {
        final Mosque mosque = mosques.get(position);
        int userId = getSession().getInt(SessionHelper.USER_ID);
        String token = getSession().getString(SessionHelper.TOKEN);
        int mosqueId = mosque.getId();

        //Call follow / unfollow API
        Call<BaseResponse> call = !mosque.isFollowed() ?
                getService().followMosque(userId, token, mosqueId) :
                getService().unfollowMosque(userId, token, mosqueId);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.code() == 200) {
                    //Save to database
                    mosque.setFollowed(!mosque.isFollowed());
                    try {
                        mainDao.update(mosque);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    adapter.notifyDataSetChanged();
                } else
                    showFollowError(mosque);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                showFollowError(mosque);
            }
        });
    }


    /**
     * Show error message for follow fetaure
     *
     * @param mosque
     */
    private void showFollowError(Mosque mosque) {
        showToast(mosque.isFollowed() ? R.string.message_failed_unfollow : R.string.message_failed_follow);
    }

    @Override
    public void onItemClicked(int position) {
        Intent intent = new Intent(getActivity(), DetailActivity.class);
        intent.putExtra(Config.Extra.ID, mosques.get(position).getId());
        startActivity(intent);
    }

    @Override
    public void onScrolledTop() {
    }

    /**
     * Load more if connected
     */
    @Override
    public void onScrolledBottom() {
        if (isNetworkConnected() && !loading && !endOfData)
            loadServerData(mosques.size());
    }

    @Override
    public void onDestroyView() {
//        EventBus.getDefault().unregister(this);
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
