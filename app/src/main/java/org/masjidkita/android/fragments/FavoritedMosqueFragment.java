package org.masjidkita.android.fragments;


import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ViewAnimator;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

import org.masjidkita.android.Config;
import org.masjidkita.android.R;
import org.masjidkita.android.activities.MainActivity;
import org.masjidkita.android.adapters.MosqueAdapter;
import org.masjidkita.android.adapters.ScrollListener;
import org.masjidkita.android.database.models.Mosque;
import org.masjidkita.android.helpers.SessionHelper;
import org.masjidkita.android.network.response.BaseResponse;
import org.masjidkita.android.network.response.MosquesResponse;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link CoreFragment} subclass.
 */
public class FavoritedMosqueFragment extends CoreFragment implements
        ScrollListener,
        MosqueAdapter.OnClickListener {
    private static int SORT_FOLLOWER = 0;
    private static int SORT_ALPHABETICALLY = 1;

    @Bind(R.id.rv_data)
    RecyclerView rvData;
    @Bind(R.id.va_main)
    ViewAnimator vaMain;
    @Bind(R.id.sr_main)
    SwipeRefreshLayout srMain;
    @Bind(R.id.sp_sort)
    Spinner spSort;

    List<Mosque> mosques = new ArrayList<>();
    private MosqueAdapter adapter;
    private Dao<Mosque, Integer> mainDao;
    private boolean loading;
    private boolean endOfData;
    private int currentSort = SORT_FOLLOWER;

    public FavoritedMosqueFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favorited_mosque, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(R.string.nav_favorited_mosque);

        setupDao();
        setupSpinnerAdapter();
        setupAdapter();
        loadData();
    }

    @OnClick(R.id.ll_failed_fetch)
    void tryFetch() {
        loadServerData(0);
    }

    /**
     * Setup spinner toolbar for sort function
     */
    private void setupSpinnerAdapter() {

        final String[] items = getResources().getStringArray(R.array.spinner_favorite_sort);
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.item_spinner_toolbar, R.id.sp_text, items) {
//            public View getView(int position, View convertView, ViewGroup parent) {
//                View v = convertView;
//                if (v == null)
//                    v = LayoutInflater.from(getContext()).inflate(R.layout.item_spinner_toolbar, parent, false);
//                TextView tv = (TextView) v.findViewById(R.id.sp_text);
//                tv.setText(items[position]);
//                return v;
//            }
//        };
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.item_list_spinner, items);
        spSort.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (view == null)
                    return;
                ((TextView) view).setTextColor(ContextCompat.getColor(getContext(), R.color.white_dark));
                view.setBackgroundResource(R.drawable.sp_toolbar_selected);
                sortData(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSort.setAdapter(adapter);
        spSort.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        spSort.setVisibility(View.VISIBLE);

        //Set text color for spinner
        spSort.setSelection(0, true);
        View view = spSort.getSelectedView();
        if (view == null)
            return;
        ((TextView) view).setTextColor(ContextCompat.getColor(getContext(), R.color.white_dark));
        view.setBackgroundResource(R.drawable.sp_toolbar_selected);
    }

    /**
     * Sort list data by position
     *
     * @param sort As sort spinner index
     */
    private void sortData(int sort) {
        currentSort = sort;
        loadLocalData();
        if (isNetworkConnected())
            loadServerData(0);
    }

    /**
     * Init all orm dao from database helper
     */
    private void setupDao() {
        try {
            mainDao = getDatabase().getDao(Mosque.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    /**
     * Load data from server / local
     */
    private void loadData() {
        loadLocalData();
        if (isNetworkConnected())
            loadServerData(0);
    }

    /**
     * Load mosque data from local with realms
     */
    private void loadLocalData() {
        int total = 0;
        try {
            total = (int) mainDao.countOf();
            // Populate data if table has data
            if (total > 0) {
                //Define current sort parameter for database query
                String orderField = Mosque.FOLLOWER;
                boolean orderValue = false;
                if (currentSort == SORT_ALPHABETICALLY) {
                    orderField = Mosque.NAME;
                    orderValue = false;
                }

                QueryBuilder<Mosque, Integer> builder = mainDao.queryBuilder();
                builder.where().eq(Mosque.FOLLOWED, true);
                builder.orderBy(orderField, orderValue);
                builder.limit((long) Config.Limit.DATA);
                mosques.addAll(builder.query());

                adapter.notifyDataSetChanged();
            }
            vaMain.setDisplayedChild(total > 0 ? LAYOUT_MAIN : LAYOUT_NO_DATA);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Load mosque data from API with retrofit
     */
    private void loadServerData(final int offset) {
        loading = true;

        int userId = getSession().getInt(SessionHelper.USER_ID);
        String token = getSession().getString(SessionHelper.TOKEN);

        showLoading();
        Call<MosquesResponse> call = getService().getFavoritedMosques(userId, token, currentSort, offset, Config.Limit.DATA);
        call.enqueue(new Callback<MosquesResponse>() {
            @Override
            public void onResponse(Call<MosquesResponse> call, Response<MosquesResponse> response) {
                loading = false;

                if (response.code() == 200) {
                    MosquesResponse mosquesResponse = response.body();

                    //Clear data on first fetch
                    if (offset == 0) {
                        endOfData = false;

                        mosques.clear();
//                        try {
//                            if (getActivity() != null) {
//                                TableUtils.clearTable(getDatabase().getConnectionSource(), Mosque.class);
//                            }
//                        } catch (SQLException e) {
//                            e.printStackTrace();
//                        }
                    }

                    //Save to database
                    try {
                        if (mosquesResponse.getData() != null)
                            for (Mosque mosque : mosquesResponse.getData()) {
                                mosque.setFollowed(true);
                                mainDao.createOrUpdate(mosque);
                            }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    if (mosquesResponse.getData() != null)
                        mosques.addAll(mosquesResponse.getData());
                    adapter.notifyDataSetChanged();

                    //Don't load
                    if (mosquesResponse.getData() == null || mosquesResponse.getData().size() == 0 && offset > 0)
                        endOfData = true;
                }
                showCompletedData();
            }

            @Override
            public void onFailure(Call<MosquesResponse> call, Throwable t) {
                showError();
            }
        });
    }

    /**
     * Show load progress if datalist equals 0
     */
    private void showLoading() {
        if (mosques.size() == 0)
            vaMain.setDisplayedChild(LAYOUT_PROGRESS);
    }

    /**
     * Set view animator visibility if necessary
     */
    private void showCompletedData() {
        if (srMain == null)
            return;
        srMain.setRefreshing(false);
        vaMain.setDisplayedChild(mosques.size() > 0 ? LAYOUT_MAIN : LAYOUT_NO_DATA);
    }

    /**
     * Show error page
     */
    private void showError() {
        if (vaMain == null)
            return;
        srMain.setRefreshing(false);
        if (mosques.size() == 0)
            vaMain.setDisplayedChild(LAYOUT_FAILED);
        else
            showToast(R.string.message_failed_fetch);
    }

    /**
     * Populate recyclerview from adapter
     */
    private void setupAdapter() {
        srMain.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadServerData(0);
            }
        });

        adapter = new MosqueAdapter(this, mosques);
        rvData.setAdapter(adapter);
        rvData.addOnScrollListener(adapter.getScrollListener(this, mosques));

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvData.setLayoutManager(layoutManager);
    }

    /**
     * Show error message for follow fetaure
     *
     * @param mosque
     */
    private void showFollowError(Mosque mosque) {
        showToast(mosque.isFollowed() ? R.string.message_failed_unfollow : R.string.message_failed_follow);
    }

    @Override
    public void onScrolledTop() {

    }

    /**
     * Load more if connected
     */
    @Override
    public void onScrolledBottom() {
        if (isNetworkConnected() && !loading && !endOfData)
            loadServerData(mosques.size());
    }

    @Override
    public void onFollowClicked(int position) {
        final Mosque mosque = mosques.get(position);
        int userId = getSession().getInt(SessionHelper.USER_ID);
        String token = getSession().getString(SessionHelper.TOKEN);
        int mosqueId = mosque.getId();

        //Call follow / unfollow API
        Call<BaseResponse> call = !mosque.isFollowed() ?
                getService().followMosque(userId, token, mosqueId) :
                getService().unfollowMosque(userId, token, mosqueId);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.code() == 200) {
                    //Save to database
                    mosque.setFollowed(!mosque.isFollowed());
                    try {
                        mainDao.update(mosque);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    adapter.notifyDataSetChanged();
                } else
                    showFollowError(mosque);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                showFollowError(mosque);
            }
        });
    }

    @Override
    public void onItemClicked(int position) {
        //TODO SHow detail masjid
    }
}
