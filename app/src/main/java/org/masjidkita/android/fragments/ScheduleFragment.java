package org.masjidkita.android.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.masjidkita.android.R;
import org.masjidkita.android.activities.MainActivity;
import org.masjidkita.android.adapters.ScrollListener;

import butterknife.ButterKnife;

/**
 * A simple {@link CoreFragment} subclass.
 */
public class ScheduleFragment extends CoreFragment implements
        ScrollListener {

    public ScheduleFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.layout_page_unavailable, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((MainActivity)getActivity()).getSupportActionBar().setTitle(R.string.nav_schedule);
//        setupDao();
//        setupSpinnerAdapter();
//        setupAdapter();
//        loadData();
    }

    @Override
    public void onScrolledTop() {

    }

    /**
     * Load more if connected
     */
    @Override
    public void onScrolledBottom() {
    }
}
