package org.masjidkita.android.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ViewAnimator;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;

import org.masjidkita.android.Config;
import org.masjidkita.android.R;
import org.masjidkita.android.activities.DetailActivity;
import org.masjidkita.android.adapters.FeedAdapter;
import org.masjidkita.android.adapters.ScrollListener;
import org.masjidkita.android.database.models.Article;
import org.masjidkita.android.database.models.Feed;
import org.masjidkita.android.database.models.FeedDetail;
import org.masjidkita.android.helpers.SessionHelper;
import org.masjidkita.android.network.response.FeedResponse;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link CoreFragment} subclass.
 */
public class FeedFragment extends CoreFragment implements
        FeedAdapter.OnClickListener,
        ScrollListener {

    @Bind(R.id.rv_data)
    RecyclerView rvData;
    @Bind(R.id.va_main)
    ViewAnimator vaMain;
    @Bind(R.id.sr_main)
    SwipeRefreshLayout srMain;

    private List<Feed> feeds = new ArrayList<>();
    private List<Article> mArticles = new ArrayList<>();
    private FeedAdapter mAdapter;
    private Dao<Feed, Integer> feedDao;
    private Dao<FeedDetail, Integer> feedDetailDao;
    private boolean loading;
    private boolean endOfData;
    private boolean fromNoConnection;

    public FeedFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        EventBus.getDefault().registerWithSocialAccount(this);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_feed, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupDao();
        setupAdapter();
        loadData();
    }


    @OnClick(R.id.ll_failed_fetch)
    void tryFetch() {
        loadServerData(0);
    }


    private void setupDao() {
        try {
            feedDao = getDatabase().getDao(Feed.class);
            feedDetailDao = getDatabase().getDao(FeedDetail.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Load data from server / local
     */
    private void loadData() {
        loadLocalData();
        if (isNetworkConnected())
            loadServerData(0);
    }

    /**
     * Load data from  local
     */
    private void loadLocalData() {
        try {
            showLoading();
            feeds.clear();
            feeds.addAll(feedDao.queryBuilder()
                    .limit((long) Config.Limit.DATA)
                    .orderBy(Feed.DATE, false)
                    .query());

            //Get feed details
            for (Feed feed : feeds)
                if (feed.getFeedDetail() == null) {
                    List<FeedDetail> details = new ArrayList<>();
                    details.addAll(feedDetailDao.queryForEq(FeedDetail.FEED_ID, feed.getId()));
                    if (details.size() > 0)
                        feed.setFeedDetail(details.get(0));
                }

            mAdapter.notifyDataSetChanged();
            showCompletedData();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Load data from server if connected, if offset 0 = load first data
     */
    private void loadServerData(final int offset) {
        loading = true;

        int userId = getSession().getInt(SessionHelper.USER_ID);
        String token = getSession().getString(SessionHelper.TOKEN);

        showLoading();
        Call<FeedResponse> call = getService().getFeeds(userId, token, offset, Config.Limit.DATA);
        call.enqueue(new Callback<FeedResponse>() {
            @Override
            public void onResponse(Call<FeedResponse> call, Response<FeedResponse> response) {
                loading = false;

                if (response.code() == 200) {
                    FeedResponse feedResponse = response.body();

                    //Clear data on first fetch
                    if (offset == 0) {
                        endOfData = false;
                        feeds.clear();
                    }

                    //Save to database
                    try {
                        //Delete feed detail
                        DeleteBuilder<FeedDetail, Integer> builder = feedDetailDao.deleteBuilder();
                        builder.where().not().eq(FeedDetail.ID, 0);
                        builder.delete();

                        //Save responses
                        if (feedResponse.getData() != null)
                            for (Feed feed : feedResponse.getData()) {
                                feedDao.createOrUpdate(feed);

                                FeedDetail detail = feed.getFeedDetail();
                                if (detail != null) {
                                    detail.setFeed(feed);
                                    feedDetailDao.createOrUpdate(detail);
                                }
                            }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                    feeds.addAll(feedResponse.getData());
                    mAdapter.notifyDataSetChanged();

                    //Don't load
                    if (feedResponse.getData().size() == 0 && offset > 0)
                        endOfData = true;
                } else
                    showToast(R.string.message_failed_fetch);
                showCompletedData();
            }

            @Override
            public void onFailure(Call<FeedResponse> call, Throwable t) {
                showError();
            }
        });
    }

    /**
     * Show load progress if datalist equals 0
     */
    private void showLoading() {
        if (feeds.size() == 0)
            vaMain.setDisplayedChild(LAYOUT_PROGRESS);
    }

    /**
     * Set view animator visibility if necessary
     */
    private void showCompletedData() {
        if (srMain == null)
            return;
        srMain.setRefreshing(false);
        vaMain.setDisplayedChild(feeds.size() > 0 ? LAYOUT_MAIN : LAYOUT_NO_DATA);
    }

    /**
     * Show error page
     */
    private void showError() {
        if (srMain == null)
            return;
        srMain.setRefreshing(false);
        if (feeds.size() == 0)
            vaMain.setDisplayedChild(LAYOUT_FAILED);
        else
            showToast(R.string.message_failed_fetch);
    }

    /**
     * Populate recyclerview from adapter
     */
    private void setupAdapter() {
        srMain.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadServerData(0);
            }
        });

        mAdapter = new FeedAdapter(this, feeds);
        rvData.setAdapter(mAdapter);
        rvData.addOnScrollListener(mAdapter.getScrollListener(this, feeds));
//        rvData.setOverScrollMode(View.OVER_SCROLL_ALWAYS);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvData.setLayoutManager(layoutManager);
    }

    @Override
    public void onMosqueClicked(int position) {
        //TODO Show mosque detail
    }

    @Override
    public void onItemClicked(int position) {
        Intent intent = new Intent(getActivity(), DetailActivity.class);
        intent.putExtra(Config.Extra.TYPE, feeds.get(position).getType());
        intent.putExtra(Config.Extra.ID, feeds.get(position).getId());
        startActivity(intent);
    }

    @Override
    public void onScrolledTop() {
    }

    /**
     * Load more if connected
     */
    @Override
    public void onScrolledBottom() {
        if (isNetworkConnected() && !loading && !endOfData)
            loadServerData(feeds.size());
    }

    @Override
    public void onDestroyView() {
//        EventBus.getDefault().unregister(this);
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}