package org.masjidkita.android.fragments;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.masjidkita.android.Config;
import org.masjidkita.android.MyApplication;
import org.masjidkita.android.database.DatabaseHelper;
import org.masjidkita.android.helpers.SessionHelper;
import org.masjidkita.android.network.ApiService;

/**
 * A simple {@link Fragment} subclass.
 */
public class CoreFragment extends Fragment {
    //View animator layout index
    public static final int LAYOUT_MAIN = 0;
    public static final int LAYOUT_PROGRESS = 1;
    public static final int LAYOUT_NO_DATA = 2;
    public static final int LAYOUT_FAILED = 3;

    public CoreFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    /**
     * Get API Service from Application
     */
    public ApiService getService() {
        return ((MyApplication) getActivity().getApplication()).getService();
    }

    /**
     * Get API Service from Application
     */
    public SessionHelper getSession() {
        return ((MyApplication) getActivity().getApplication()).getSession();
    }

    /**
     * Get realms object for database
     *
     * @return
     */
    public DatabaseHelper getDatabase() {
        return ((MyApplication) getActivity().getApplication()).getDatabase();
    }

    protected boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null;
    }

    protected void showToast(int resId) {
        Toast.makeText(getContext(), resId, Toast.LENGTH_SHORT).show();
    }

    protected void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    protected void logE(String message){
        Log.e(Config.TAG, message);
    }
}
