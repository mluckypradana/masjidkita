package org.masjidkita.android.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.masjidkita.android.R;
import org.masjidkita.android.activities.MainActivity;
import org.masjidkita.android.adapters.ViewPagerAdapter;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends CoreFragment {


    @Bind(R.id.tl_main)
    TabLayout tlHome;
    @Bind(R.id.vp_main)
    ViewPager vpHome;
    private ViewPagerAdapter adapter;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((MainActivity)getActivity()).getSupportActionBar().setTitle(R.string.app_name);
        setupViewPager(vpHome);
    }

    /**
     * Populate fragments inside viewpager
     *
     * @param viewPager
     */
    private void setupViewPager(ViewPager viewPager) {
        //Prevent first fragment not showing up (Don't reinitiate adpater)
//        if(adapter!=null)
//            return;

        adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        tlHome.setupWithViewPager(viewPager);
//        tlMain.addTab(tlMain.newTab().setText(R.string.title_tab_all));
        FeedFragment feedFragment = new FeedFragment();
        ArticleFragment articleFragment = new ArticleFragment();
        EventFragment eventFragment = new EventFragment();
        InfaqFragment infaqFragment = new InfaqFragment();
        ModuleChildFragment moduleChildFragment = new ModuleChildFragment();
        adapter.addFragment(feedFragment, getString(R.string.title_tab_all));
        adapter.addFragment(articleFragment, getString(R.string.title_tab_article));
        adapter.addFragment(eventFragment, getString(R.string.title_tab_activity));
        adapter.addFragment(infaqFragment, getString(R.string.title_tab_infaq));
        adapter.addFragment(moduleChildFragment, getString(R.string.title_tab_module));
        viewPager.setAdapter(adapter);
        viewPager.getAdapter().notifyDataSetChanged();
        viewPager.destroyDrawingCache();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
