package org.masjidkita.android.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.masjidkita.android.Config;
import org.masjidkita.android.R;
import org.masjidkita.android.database.models.Event;
import org.masjidkita.android.database.models.Feed;
import org.masjidkita.android.helpers.DateHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by MuhammadLucky on 13/08/2016.
 * All type of objects are here
 */
public class EventAdapter extends CoreAdapter {
    private OnClickListener mListener;
    private List<Event> mEvents = new ArrayList<>();
    private CropCircleTransformation mCircleTransform;

    public EventAdapter(OnClickListener context, List<Event> events) {
        mEvents = events;
        mListener = context;
    }
    //TODO News list to count

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        setupAdapter(parent);
        mCircleTransform = new CropCircleTransformation(context);
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_feed, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder baseHolder, int position) {
        Event event = mEvents.get(position);
        ViewHolder holder = (ViewHolder) baseHolder;

        //Customize fonts
        customizeFonts(Config.Font.DEFAULT, holder.tvDate, holder.tvTitle, holder.tvContent,
                holder.tvDetail1, holder.tvDetail2, holder.tvLabel1, holder.tvLabel2);
        customizeFonts(Config.Font.BOLD, holder.tvMosque, holder.tvAction1, holder.tvAction2);

        //Set visiblity every component
        String type = Feed.TYPE_EVENT;
        String date = event.getDate();
        String title = event.getName();
        String excerpt = event.getExcerpt();
        if (excerpt == null)
            excerpt = "";
        String detail1 = event.getUstadz();
        String detail2 = event.getTime();

        //Show detail 1 for event / infaq type
        boolean showDetail1 = type.equals(Feed.TYPE_EVENT) || type.equals(Feed.TYPE_INFAQ);
        //Show detail 2 for event type
        boolean showDetail2 = type.equals(Feed.TYPE_EVENT);
        holder.tvContent.setVisibility(excerpt.equals("") ? View.GONE : View.VISIBLE);
        holder.llDetail.setVisibility(type.equals(Feed.TYPE_ARTICLE) ? View.GONE : View.VISIBLE);
        holder.tvLabel1.setVisibility(showDetail1 ? View.VISIBLE : View.GONE);
        holder.tvDetail1.setVisibility(showDetail1 ? View.VISIBLE : View.GONE);
        holder.tvLabel2.setVisibility(showDetail2 ? View.VISIBLE : View.GONE);
        holder.tvDetail2.setVisibility(showDetail2 ? View.VISIBLE : View.GONE);
        holder.llAction.setVisibility(type.equals(Feed.TYPE_EVENT) ? View.VISIBLE : View.GONE);
        holder.ivThumb.setVisibility(type.equals(Feed.TYPE_ARTICLE) ? View.VISIBLE : View.GONE);
        holder.tvDate.setVisibility(View.VISIBLE);

//        Glide.with(context).load(event.getThumbnail()).placeholder(R.mipmap.tmb_default_event).into(holder.ivThumb);
//        Glide.with(context).load(R.mipmap.tmb_default_mosque).bitmapTransform(mCircleTransform).into(holder.ivMosqueThumb);
        //TODO Load mosque thumb
        holder.tvMosque.setText(event.getMasjid());
        String dateText = DateHelper.parseDate(date);
        holder.tvDate.setText(dateText);
        holder.tvTitle.setText(title);
        holder.tvContent.setText(excerpt);
        holder.tvLabel1.setText(R.string.label_event_ustadz);
        holder.tvLabel2.setText(R.string.label_event_hour);
        holder.tvDetail1.setText(detail1);
        holder.tvDetail2.setText(detail2);
    }

    @Override
    public int getItemCount() {
        return mEvents.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.iv_thumb)
        ImageView ivThumb;
        @Bind(R.id.tv_mosque)
        TextView tvMosque;
        @Bind(R.id.tv_date)
        TextView tvDate;
        @Bind(R.id.tv_title)
        TextView tvTitle;
        @Bind(R.id.tv_content)
        TextView tvContent;
        @Bind(R.id.tv_label_1)
        TextView tvLabel1;
        @Bind(R.id.tv_detail_1)
        TextView tvDetail1;
        @Bind(R.id.tv_label_2)
        TextView tvLabel2;
        @Bind(R.id.tv_detail_2)
        TextView tvDetail2;
        @Bind(R.id.ll_detail)
        LinearLayout llDetail;
        @Bind(R.id.ll_action)
        LinearLayout llAction;
        @Bind(R.id.cv_main)
        CardView cvMain;
        @Bind(R.id.tv_action_1)
        TextView tvAction1;
        @Bind(R.id.tv_action_2)
        TextView tvAction2;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        @OnClick(R.id.ll_main)
        void showDetail() {
            if (mListener != null)
                mListener.onItemClicked(getAdapterPosition());
        }
    }

    public interface OnClickListener {
        void onMosqueClicked(int position);

        void onItemClicked(int position);
    }
}
