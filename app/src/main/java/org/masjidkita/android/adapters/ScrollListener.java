package org.masjidkita.android.adapters;

/**
 * Created by MuhammadLucky on 24/08/2016.
 */
public interface ScrollListener {
    void onScrolledTop();

    void onScrolledBottom();
}
