package org.masjidkita.android.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.masjidkita.android.MyApplication;
import org.masjidkita.android.database.DatabaseHelper;
import org.masjidkita.android.database.models.CoreModel;
import org.masjidkita.android.helpers.SessionHelper;
import org.masjidkita.android.network.ApiService;

import java.util.List;

/**
 * Created by MuhammadLucky on 01-Oct-15
 */

public class CoreAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    protected String version;
    protected Context context;
    //    protected ApiService service;
//    protected DatabaseHelper helper;
//    protected SessionManagement session;
    protected DisplayMetrics displayMetrics;
    protected View view;
    //For scroll lsitener
    protected List<T> mDataList;
    private ScrollListener mListener;

    public CoreAdapter() {
    }

    /**
     * Init api service, database helper, session management etc.
     */
    protected void setupAdapter(ViewGroup parent) {
        context = parent.getContext();
        if (displayMetrics == null) {
            //Init display metric
            displayMetrics = new DisplayMetrics();
            ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    protected boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null;
    }

    public ApiService getService() {
        return ((MyApplication) context.getApplicationContext()).getService();
    }

    public DatabaseHelper getHelper() {
        return ((MyApplication) context.getApplicationContext()).getDatabase();
    }

    public SessionHelper getSession() {
        return ((MyApplication) context.getApplicationContext()).getSession();
    }

    /**
     * Add scroll listener with custom interface ScrollListener
     *
     * @param listener As fragmnent, implemented with listener
     * @param dataList As list of object
     * @return Scroll listener for recyclerview
     */
    public RecyclerView.OnScrollListener getScrollListener(final ScrollListener listener, List<T> dataList) {
        mDataList = dataList;
        mListener = listener;
        return new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager mLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                //Scroll down
                if (dy > 0) {
                    //If position equals to last (two) position
                    int currentLastItemPosition = mLayoutManager.findLastVisibleItemPosition();
                    if (mDataList.size() > 1 && currentLastItemPosition >= mDataList.size() - 2 && mListener != null)
                        mListener.onScrolledBottom();
                } else {
                    //If position equals to first position
                    int currentFirstItemPosition = mLayoutManager.findFirstCompletelyVisibleItemPosition();
                    if (currentFirstItemPosition == 0 && mListener != null)
                        mListener.onScrolledTop();
                }
            }
        };
    }

    /**
     * Customize font to specific font face.
     *
     * @param textViews As {@link TextView} component
     */
    protected void customizeFonts(String fontFace, TextView... textViews) {
        Typeface typeFace = Typeface.createFromAsset(context.getAssets(), fontFace);
        for (TextView textView : textViews) {
            textView.setTypeface(typeFace);
            textView.invalidate();
        }
    }
}

