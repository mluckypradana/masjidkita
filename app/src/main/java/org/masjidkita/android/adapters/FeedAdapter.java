package org.masjidkita.android.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.florent37.glidepalette.GlidePalette;

import org.masjidkita.android.Config;
import org.masjidkita.android.R;
import org.masjidkita.android.database.models.Article;
import org.masjidkita.android.database.models.Feed;
import org.masjidkita.android.helpers.CurrencyHelper;
import org.masjidkita.android.helpers.DateHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by MuhammadLucky on 13/08/2016.
 * All type of objects are here
 */
public class FeedAdapter extends CoreAdapter {
    private OnClickListener listener;
    private List<Feed> mFeeds = new ArrayList<>();
    private List<Article> mArticles;
    private CropCircleTransformation mCircleTransform;

    public FeedAdapter(OnClickListener listener, List<Feed> feeds) {
        mFeeds = feeds;
        this.listener = listener;
    }


    //TODO News list to count

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        setupAdapter(parent);
        mCircleTransform = new CropCircleTransformation(context);
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_feed, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder baseHolder, int position) {
        Feed feed = mFeeds.get(position);
        ViewHolder holder = (ViewHolder) baseHolder;

//        //Remove margin if position equals first position
//        int padding = context.getResources().getDimensionPixelSize(R.dimen.padding);
//        int paddingTop = position == 0 ? padding : 0;
//        RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) holder.cvMain.getLayoutParams();
//        param.setMargins(padding, paddingTop, padding, padding);

        customizeFonts(Config.Font.DEFAULT, holder.tvDate, holder.tvTitle, holder.tvContent,
                holder.tvDetail1, holder.tvDetail2, holder.tvLabel1, holder.tvLabel2);
        customizeFonts(Config.Font.BOLD, holder.tvMosque);//, holder.tvAction1, holder.tvAction2);

        //Init variables
        String type = feed.getType();
        String date = feed.getDate();
        String title = feed.getName();
        String excerpt = feed.getExcerpt();
        String thumbnail = feed.getThumbnail();

        //Set visiblity every component
        //Show detail 1 for event / infaq type
        boolean showDetail1 = type.equals(Feed.TYPE_EVENT) || type.equals(Feed.TYPE_INFAQ);
        //Show detail 2 for event type
        boolean showDetail2 = false;// type.equals(Feed.TYPE_EVENT);
        holder.tvContent.setVisibility(feed.getExcerpt().equals("") ? View.GONE : View.VISIBLE);
        holder.llDetail.setVisibility(showDetail1 ? View.VISIBLE : View.GONE);
        holder.tvLabel1.setVisibility(showDetail1 ? View.VISIBLE : View.GONE);
        holder.tvDetail1.setVisibility(holder.tvLabel1.getVisibility());
        holder.tvLabel2.setVisibility(showDetail2 ? View.VISIBLE : View.GONE);
        holder.tvDetail2.setVisibility(holder.tvLabel2.getVisibility());
        holder.llAction.setVisibility(type.equals(Feed.TYPE_EVENT) ? View.VISIBLE : View.GONE);
        holder.llSeparator.setVisibility(holder.llAction.getVisibility());
        holder.tvDate.setVisibility(View.VISIBLE);

        holder.ivThumb.setVisibility(type.equals(Feed.TYPE_ARTICLE) ? View.VISIBLE : View.GONE);
        Glide.with(context)
                .load(thumbnail)
                .listener(GlidePalette.with(thumbnail)
                        .use(Config.GLIDE_PALETTE_PROFILE)
                        .intoBackground(holder.ivThumb)
                        .crossfade(true)
                )
                .into(holder.ivThumb);

        holder.tvMosque.setText(feed.getMasjid());
        String dateText = DateHelper.parseDate(date);
        holder.tvDate.setText(dateText);
        holder.tvTitle.setText(title);
        holder.tvContent.setText(excerpt);

        //Set details
        if (showDetail1 && feed.getFeedDetail() != null) {
            if (feed.getFeedDetail().getTime() != null) {
                //For event hour
                holder.tvLabel1.setText(R.string.label_event_hour);
                String time = feed.getFeedDetail().getTime();
                holder.tvDetail1.setText(time);
            } else {
                //For infaq target
                holder.tvLabel1.setText(R.string.label_infaq_target);
                String target = CurrencyHelper.parseCurrency(feed.getFeedDetail().getTarget());
                holder.tvDetail1.setText("Rp." + target.replace(",", "."));
            }
        } else
            holder.llDetail.setVisibility(View.GONE);

        //For event date
        if (showDetail2) {
            holder.tvLabel2.setText(R.string.label_event_date);
            holder.tvLabel1.setText(R.string.label_event_date);
        }

        //Set item type
        int resId = R.mipmap.ic_article;
        if (type.equals(Feed.TYPE_EVENT))
            resId = R.mipmap.ic_event;
        else if (type.equals(Feed.TYPE_INFAQ))
            resId = R.mipmap.ic_coin;
        else if (type.equals(Feed.TYPE_MODULE))
            resId = R.mipmap.ic_module;
        holder.ivType.setVisibility(View.VISIBLE);
        holder.ivType.setImageResource(resId);
    }

    @Override
    public int getItemCount() {
        return mFeeds.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.iv_thumb)
        ImageView ivThumb;
        @Bind(R.id.iv_type)
        ImageView ivType;
        @Bind(R.id.tv_mosque)
        TextView tvMosque;
        @Bind(R.id.tv_date)
        TextView tvDate;
        @Bind(R.id.tv_title)
        TextView tvTitle;
        @Bind(R.id.tv_content)
        TextView tvContent;
        @Bind(R.id.tv_label_1)
        TextView tvLabel1;
        @Bind(R.id.tv_detail_1)
        TextView tvDetail1;
        @Bind(R.id.tv_label_2)
        TextView tvLabel2;
        @Bind(R.id.tv_detail_2)
        TextView tvDetail2;
        @Bind(R.id.ll_detail)
        LinearLayout llDetail;
        @Bind(R.id.ll_action)
        LinearLayout llAction;
        @Bind(R.id.ll_separator)
        LinearLayout llSeparator;
//        @Bind(R.id.cv_main)
//        CardView cvMain;
//        @Bind(R.id.tv_action_1)
//        TextView tvAction1;
//        @Bind(R.id.tv_action_2)
//        TextView tvAction2;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        @OnClick(R.id.ll_main)
        void showDetail() {
            if (listener != null)
                listener.onItemClicked(getAdapterPosition());
        }
    }

    public interface OnClickListener {
        void onMosqueClicked(int position);

        void onItemClicked(int position);
    }
}
