package org.masjidkita.android.adapters;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.florent37.glidepalette.GlidePalette;

import org.masjidkita.android.Config;
import org.masjidkita.android.R;
import org.masjidkita.android.database.models.Mosque;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by MuhammadLucky on 13/08/2016.
 * All type of objects are here
 */
public class MosqueAdapter extends CoreAdapter {

    private List<Mosque> mosques = new ArrayList<>();
    private OnClickListener listener;

    public MosqueAdapter(OnClickListener listener, List<Mosque> mosques) {
        this.listener = listener;
        this.mosques = mosques;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        setupAdapter(parent);
//        circleTransform = new CropCircleTransformation(context);
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mosque, parent, false);

        ViewHolder holder = new ViewHolder(view);
        customizeFonts(Config.Font.LIGHT, holder.tTitle);
        customizeFonts(Config.Font.BOLD, holder.tFollower);

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder baseHolder, int position) {
        Mosque mosque = mosques.get(position);
        ViewHolder holder = (ViewHolder) baseHolder;

//        //Remove padding if position equals first position
//        int padding = context.getResources().getDimensionPixelSize(R.dimen.padding_small);
//        int paddingTop = position == 0 ? padding : 0;
//        CoordinatorLayout.LayoutParams param = (CoordinatorLayout.LayoutParams) holder.ivMosque.getLayoutParams();
//        param.setMargins(padding, paddingTop, padding, padding);

        holder.tTitle.setText(mosque.getName());
        //Hide detail if address is null
        if (mosque.getAddress() == null)
            holder.llDetail.setVisibility(View.GONE);
        else {
            holder.tAddress.setText(mosque.getAddress());
            holder.tDescription.setText(mosque.getDescription());
        }

        //Set follower text
        int followerTotal = mosque.getFollower();
        String followerTotalText = followerTotal > 0 ?
                MessageFormat.format(context.getResources().getString(R.string.label_has_follower), mosque.getFollower()) :
                context.getString(R.string.label_no_follower);
        if (followerTotal >= 1000)
            followerTotalText = ((float) followerTotal / 1000) + "K";
        holder.tFollower.setText(followerTotalText);

        //Setup follow button
        holder.ivFollow.setImageResource(
                mosque.isFollowed() ?
                        R.mipmap.ic_favorite_inverse :
                        R.mipmap.ic_favorite_border_inverse);

        String thumbnail = mosque.getThumbnail();
        if (thumbnail != null && !thumbnail.isEmpty())
            Glide.with(context)
                    .load(thumbnail)
                    .listener(GlidePalette.with(thumbnail)
                            .use(Config.GLIDE_PALETTE_PROFILE)
                            .intoBackground(holder.ivMosque)
                            .crossfade(true)
                    )
                    .into(holder.ivMosque);
    }

    @Override
    public int getItemCount() {
        return mosques.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.iv_mosque)
        ImageView ivMosque;
        @Bind(R.id.t_title)
        TextView tTitle;
        @Bind(R.id.t_follower)
        TextView tFollower;
        @Bind(R.id.iv_follow)
        FloatingActionButton ivFollow;
        @Bind(R.id.cl_main)
        CoordinatorLayout clMain;
        @Bind(R.id.t_address)
        TextView tAddress;
        @Bind(R.id.t_description)
        TextView tDescription;
        @Bind(R.id.ll_detail)
        LinearLayout llDetail;

        @OnClick(R.id.iv_follow)
        void followMosque() {
            //Set layout
            Mosque mosque = mosques.get(getAdapterPosition());
            if (isNetworkConnected())
                ivFollow.setImageResource(mosque.isFollowed() ?
                        R.mipmap.ic_favorite_border_inverse : R.mipmap.ic_favorite_inverse);

            //Call listener
            if (listener != null)
                listener.onFollowClicked(getAdapterPosition());
        }

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public interface OnClickListener {
        void onFollowClicked(int position);

        void onItemClicked(int position);
    }
}
