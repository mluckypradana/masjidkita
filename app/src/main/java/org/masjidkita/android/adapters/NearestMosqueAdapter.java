package org.masjidkita.android.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.masjidkita.android.Config;
import org.masjidkita.android.R;
import org.masjidkita.android.database.models.Mosque;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by MuhammadLucky on 13/08/2016.
 * All type of objects are here
 */
public class NearestMosqueAdapter extends CoreAdapter {
    private List<Mosque> mosques = new ArrayList<>();
    private OnClickListener listener;

    public NearestMosqueAdapter(OnClickListener listener, List<Mosque> mosques) {
        this.listener = listener;
        this.mosques = mosques;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        setupAdapter(parent);
//        circleTransform = new CropCircleTransformation(context);
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_nearest_mosque, parent, false);

        ViewHolder holder = new ViewHolder(view);
        customizeFonts(Config.Font.LIGHT, holder.tTitle);

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder baseHolder, int position) {
        Mosque mosque = mosques.get(position);
        ViewHolder holder = (ViewHolder) baseHolder;

        //Remove padding if position equals first position
//        int margin = context.getResources().getDimensionPixelSize(R.dimen.card_margin);
//        int marginSmall = context.getResources().getDimensionPixelSize(R.dimen.card_margin_small);
//        int marginTop = position == 0 ? margin : marginSmall;
//        int marginBottom = position == mosques.size() - 1 ? margin : marginSmall;
//        RecyclerView.LayoutParams param = (RecyclerView.LayoutParams) holder.llMain.getLayoutParams();
//        param.setMargins(margin, marginTop, margin, marginBottom);

        customizeFonts(Config.Font.LIGHT, holder.tvDistance);

        holder.tTitle.setText(mosque.getName());
        //Hide detail if address is null
        if (mosque.getAddress() != null)
            holder.tAddress.setText(mosque.getAddress());
        String distance = MessageFormat.format(
                context.getResources().getString(R.string.label_distance),
                (int) mosque.getDistance());
        holder.tvDistance.setText(distance);
    }

    @Override
    public int getItemCount() {
        return mosques.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.ll_main)
        CardView llMain;
        @Bind(R.id.t_title)
        TextView tTitle;
        @Bind(R.id.t_address)
        TextView tAddress;
        @Bind(R.id.tv_distance)
        TextView tvDistance;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public interface OnClickListener {

        void onItemClicked(int position);
    }
}
