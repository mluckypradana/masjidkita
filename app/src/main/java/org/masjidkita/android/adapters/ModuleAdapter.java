package org.masjidkita.android.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.masjidkita.android.Config;
import org.masjidkita.android.R;
import org.masjidkita.android.database.models.Feed;
import org.masjidkita.android.database.models.Module;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by MuhammadLucky on 13/08/2016.
 * All type of objects are here
 */
public class ModuleAdapter extends CoreAdapter {
    private OnClickListener listener;
    private List<Module> modules = new ArrayList<>();
    private CropCircleTransformation mCircleTransform;

    public ModuleAdapter(OnClickListener listener, List<Module> modules) {
        this.listener = listener;
        this.modules = modules;
    }


    //TODO News list to count

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        setupAdapter(parent);
        mCircleTransform = new CropCircleTransformation(context);
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_feed, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder baseHolder, int position) {
        Module module = modules.get(position);
        ViewHolder holder = (ViewHolder) baseHolder;

        //Customize fonts
        customizeFonts(Config.Font.DEFAULT, holder.tvDate, holder.tvTitle, holder.tvContent,
                holder.tvDetail1, holder.tvDetail2, holder.tvLabel1, holder.tvLabel2);
        customizeFonts(Config.Font.BOLD, holder.tvMosque, holder.tvAction1, holder.tvAction2);

        //Set visiblity every component
        String type = Feed.TYPE_MODULE;
        String title = module.getTitle();
        String thumb = module.getThumbnail();
        String detail1 = module.getCategory();
        String detail2 = context.getResources().getString(R.string.label_module_hit)
                + MessageFormat.format(
                context.getResources().getString(R.string.label_read_times),
                module.getHit());

        //Show detail 1
        boolean showDetail1 = false;
        //Show detail 2
        boolean showDetail2 = false;
        holder.tvContent.setVisibility(View.GONE);
        holder.llDetail.setVisibility(View.GONE);
        holder.tvLabel1.setVisibility(showDetail1 ? View.VISIBLE : View.GONE);
        holder.tvDetail1.setVisibility(showDetail1 ? View.VISIBLE : View.GONE);
        holder.tvLabel2.setVisibility(showDetail2 ? View.VISIBLE : View.GONE);
        holder.tvDetail2.setVisibility(showDetail2 ? View.VISIBLE : View.GONE);
        holder.llAction.setVisibility(View.GONE);
        holder.tvDate.setVisibility(View.VISIBLE);
        holder.ivThumb.setVisibility(View.VISIBLE);
        holder.tvMosque.setVisibility(View.VISIBLE);
        holder.tvTitle.setText(title);

        //Set details
        holder.tvLabel1.setText(R.string.label_module_category);
        holder.tvMosque.setText(detail1);
        if (module.getHit() > 0) {
            holder.tvLabel2.setText(R.string.label_module_hit);
            holder.tvDate.setText(detail2);
        } else {
            holder.tvDate.setVisibility(View.GONE);
            holder.tvDetail2.setVisibility(View.GONE);
        }

        Glide.with(context).load(thumb).into(holder.ivThumb);
        //.placeholder(R.mipmap.tmb_default_article)
    }

    @Override
    public int getItemCount() {
        return modules.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.iv_thumb)
        ImageView ivThumb;
        @Bind(R.id.tv_mosque)
        TextView tvMosque;
        @Bind(R.id.tv_date)
        TextView tvDate;
        @Bind(R.id.tv_title)
        TextView tvTitle;
        @Bind(R.id.tv_content)
        TextView tvContent;
        @Bind(R.id.ll_action)
        LinearLayout llAction;
        @Bind(R.id.tv_label_1)
        TextView tvLabel1;
        @Bind(R.id.tv_detail_1)
        TextView tvDetail1;
        @Bind(R.id.tv_label_2)
        TextView tvLabel2;
        @Bind(R.id.tv_detail_2)
        TextView tvDetail2;
        @Bind(R.id.ll_detail)
        LinearLayout llDetail;
        @Bind(R.id.tv_action_1)
        TextView tvAction1;
        @Bind(R.id.tv_action_2)
        TextView tvAction2;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        @OnClick(R.id.ll_main)
        void showDetail() {
            if (listener != null)
                listener.onItemClicked(getAdapterPosition());
        }
    }

    public interface OnClickListener {
        void onMosqueClicked(int position);

        void onItemClicked(int position);
    }
}
