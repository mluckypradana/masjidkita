package org.masjidkita.android;

import com.github.florent37.glidepalette.GlidePalette;

/**
 * Created by MuhammadLucky on 12/08/2016.
 */
public class Config {
    public static String TAG = "MasjidKitaTag";
    public static int GLIDE_PALETTE_PROFILE = GlidePalette.Profile.VIBRANT;
    public static int MAX_DISTANCE = 10000;//In kilometer

    public static class OrmConfig {
        public static String NAME = "MasjidKitaDB";
        public static int VERSION = 7;
        public static String MODEL_PACKAGE = "org.masjidkita.android.database.models";
    }

    //Limit data
    public static class Limit {
        public static int DATA = 10;
    }

    //In mili second
    public static class Time {
        public static int SPLASH_SCREEN = 3000;
    }

    //Fonts
    public static class Font {
        public static final String LIGHT = "roboto_thin.ttf";
        public static final String DEFAULT = "roboto_light.ttf";
        public static final String BOLD = "roboto_medium.ttf";
    }
    //    public static final String LIGHT_FONT = "avenir_light.ttf";
//    public static final String MEDIUM_FONT = "avenir_medium.ttf";
//    public static final String HEAVY_FONT = "avenir_heavy.otf";
//    //Light Font
//    public static final String BOOK_FONT = "avenir_book.ttf";

    public static class Extra {
        public static String TYPE = "TYPE";
        public static String ID = "DATA_ID";
        public static String MESSAGE = "MESSAGE";
    }

    public static class Pref {
        public static String NAME = "MasjidKitaPref";
        public static int PRIVATE_MODE = 0;
    }
}
