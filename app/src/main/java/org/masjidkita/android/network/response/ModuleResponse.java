package org.masjidkita.android.network.response;

import org.masjidkita.android.database.models.Infaq;
import org.masjidkita.android.database.models.Module;

import java.util.List;

/**
 * Created by MuhammadLucky on 26-Oct-15.
 */
public class ModuleResponse extends BaseResponse {
    private Module data;

    public Module getData() {
        return data;
    }
}
