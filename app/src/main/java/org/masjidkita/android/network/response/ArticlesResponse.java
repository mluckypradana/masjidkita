package org.masjidkita.android.network.response;

import org.masjidkita.android.database.models.Article;
import org.masjidkita.android.database.models.Feed;

import java.util.List;

/**
 * Created by MuhammadLucky on 26-Oct-15.
 */
public class ArticlesResponse extends BaseResponse {
    private List<Article> data;

    public List<Article> getData() {
        return data;
    }
}
