package org.masjidkita.android.network.request;

/**
 *
 * Created by MuhammadLucky on 26-Oct-15.
 */
public class BaseRequest {
    private String access_token;
    private String app_version;
    private String device_type="Android";

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getApp_version() {
        return app_version;
    }

    public void setApp_version(String app_version) {
        this.app_version = app_version;
    }
}
