package org.masjidkita.android.network;

import org.masjidkita.android.network.response.ArticleResponse;
import org.masjidkita.android.network.response.ArticlesResponse;
import org.masjidkita.android.network.response.BaseResponse;
import org.masjidkita.android.network.response.EventResponse;
import org.masjidkita.android.network.response.EventsResponse;
import org.masjidkita.android.network.response.FeedResponse;
import org.masjidkita.android.network.response.InfaqResponse;
import org.masjidkita.android.network.response.InfaqsResponse;
import org.masjidkita.android.network.response.LoginResponse;
import org.masjidkita.android.network.response.ModuleResponse;
import org.masjidkita.android.network.response.ModulesResponse;
import org.masjidkita.android.network.response.MosquesResponse;
import org.masjidkita.android.network.response.RegisterResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by MuhammadLucky on 6/4/2015.
 * <p/>
 * GET with @Query
 * FormURLEncoded with @Field
 * Customize URL with @Path
 */
public interface ApiService {

    @POST("register")
    Call<RegisterResponse> register(
            @Query("email") String email,
            @Query("password") String password,//m^m SHA1
            @Query("repassword") String repassword//m^m SHA1
    );

    @POST("register")
    Call<RegisterResponse> registerWithSocialAccount(
            @Query("type") String type,
            @Query("api_token") String token,
            @Query("id") String id,
            @Query("email") String email,
            @Query("password") String password//m^m SHA1
    );

    @POST("login")
    Call<LoginResponse> login(
            @Query("email") String email,
            @Query("password") String password
    );

    /**
     * For FeedFragment get all feed (article, ativities, infaq, module)
     * This API need "not null" parameters, otherwise 500
     *
     * @param userId As session user id
     * @param token  as session token get from API
     * @return List of feeds
     */
    @POST("public/home")
    Call<FeedResponse> getFeeds(
            @Query("user_id") Integer userId,
            @Query("api_token") String token,
            @Query("offset") Integer offset,
            @Query("limit") Integer limit
    );

    @POST("public/article")
    Call<ArticlesResponse> getArticles(
            @Query("user_id") Integer userId,
            @Query("api_token") String token,
            @Query("offset") Integer offset,
            @Query("limit") Integer limit
    );

    @POST("public/event")
    Call<EventsResponse> getEvents(
            @Query("user_id") Integer userId,
            @Query("api_token") String token,
            @Query("offset") Integer offset,
            @Query("limit") Integer limit
    );

    @POST("public/infaq")
    Call<InfaqsResponse> getInfaqs(
            @Query("user_id") Integer userId,
            @Query("api_token") String token,
            @Query("offset") Integer offset,
            @Query("limit") Integer limit
    );

    //TODO Ask about seed
    @POST("public/modul")
    Call<ModulesResponse> getModules(
            @Query("user_id") Integer userId,
            @Query("api_token") String token,
            @Query("offset") Integer offset,
            @Query("limit") Integer limit,
            @Query("seed") Integer seed
    );

    /**
     * For feedDetailFragment
     */
    @POST("public/article/{id}")
    Call<ArticleResponse> getArticle(
            @Path("id") Integer id,
            @Query("user_id") Integer userId,
            @Query("api_token") String token
    );

    @POST("public/event/{id}")
    Call<EventResponse> getEvent(
            @Path("id") Integer id,
            @Query("user_id") Integer userId,
            @Query("api_token") String token
    );

    @POST("public/infaq/{id}")
    Call<InfaqResponse> getInfaq(
            @Path("id") Integer id,
            @Query("user_id") Integer userId,
            @Query("api_token") String token
    );

    @POST("public/modul/{id}")
    Call<ModuleResponse> getModule(
            @Path("id") Integer id,
            @Query("user_id") Integer userId,
            @Query("api_token") String token,
            @Query("seed") Integer seed
    );

    @POST("public/semuamasjid")
    Call<MosquesResponse> getMosques(
            @Query("user_id") Integer userId,
            @Query("api_token") String token,
            @Query("offset") Integer offset,
            @Query("limit") Integer limit
    );

    @POST("public/masjidfavorit")
    Call<MosquesResponse> getFavoritedMosques(
            @Query("user_id") Integer userId,
            @Query("api_token") String token,
            @Query("filter") Integer sortAlphabetically,
            @Query("offset") Integer offset,
            @Query("limit") Integer limit
    );

    /**
     * Distance is in km
     *
     * @return
     */
    @POST("public/masjidterdekat")
    Call<MosquesResponse> getNearestMosques(
            @Query("user_id") Integer userId,
            @Query("api_token") String token,
            @Query("latitude") String latitude,
            @Query("longitude") String longitude,
            @Query("distance") Integer distance,
            @Query("offset") Integer offset,
            @Query("limit") Integer limit
    );

    @POST("public/followmasjid")
    Call<BaseResponse> followMosque(
            @Query("user_id") Integer userId,
            @Query("api_token") String token,
            @Query("masjid_id") Integer id
    );

    @POST("public/unfollowmasjid")
    Call<BaseResponse> unfollowMosque(
            @Query("user_id") Integer userId,
            @Query("api_token") String token,
            @Query("masjid_id") Integer id
    );

    /**
     * Get mosques list with location
     *
     * @param distance As distance from current location in kilometer
     * @return
     */
    @POST("public/rekomendasikanmasjid")
    Call<MosquesResponse> getMosquesRecommendation(
            @Query("user_id") Integer userId,
            @Query("api_token") String token,
            @Query("latitude") String latitude,
            @Query("longitude") String longitude,
            @Query("distance") Integer distance
    );

    /**
     * Post new mosque with caretaker data, recommend only
     */
    @POST("public/rekomendasikanmasjid/form")
    Call<BaseResponse> postMosqueRecommendation(
            @Query("user_id") Integer userId,
            @Query("api_token") String token,
            @Query("latitude") String latitude,
            @Query("longitude") String longitude,
            @Query("address") String address,
            @Query("name") String mosqueName,
            @Query("mobile") String mobile,
            @Query("pengurus") String caretaker
    );

    /**
     * Get articles for menu Article (Not tab)
     * Filter 0 for followed mosque
     * Filter 1 for all mosque
     */
    @POST("public/akun/article")
    Call<ArticlesResponse> getMyArticles(
            @Query("user_id") Integer userId,
            @Query("api_token") String token,
            @Query("offset") Integer offset,
            @Query("limit") Integer limit,
            @Query("filter") Integer filter
    );

    /**
     * Get events for menu Event (Not tab)
     * Filter 0 for followed mosque
     * Filter 1 for all mosque
     * Filter 2 for my mosque
     */
    @POST("public/akun/event")
    Call<EventsResponse> getMyEvents(
            @Query("user_id") Integer userId,
            @Query("api_token") String token,
            @Query("offset") Integer offset,
            @Query("limit") Integer limit,
            @Query("filter") Integer filter
    );

    /**
     * Get infaqs for menu Infaq (Not tab)
     * Filter 0 for followed mosque
     * Filter 1 for all mosque
     * Filter 2 my donations
     */
    @POST("public/akun/infaq")
    Call<InfaqsResponse> getMyInfaqs(
            @Query("user_id") Integer userId,
            @Query("api_token") String token,
            @Query("offset") Integer offset,
            @Query("limit") Integer limit,
            @Query("filter") Integer filter
    );

    /**
     * Get user modules
     * menu 'download' for downloaded modules
     * menu 'upload' for uploaded modules (for VIP members)
     */
    @POST("public/akun/modul")
    Call<ModulesResponse> getMyModules(
            @Query("user_id") Integer userId,
            @Query("api_token") String token,
            @Query("offset") Integer offset,
            @Query("limit") Integer limit,
            @Query("menu") String filter
    );

    @GET("public/mtw")
    Call<ResponseBody> getMtwContent();
}
