package org.masjidkita.android.network.response;

import org.masjidkita.android.database.models.Infaq;

import java.util.List;

/**
 * Created by MuhammadLucky on 26-Oct-15.
 */
public class InfaqsResponse extends BaseResponse {
    private List<Infaq> data;

    public List<Infaq> getData() {
        return data;
    }
}
