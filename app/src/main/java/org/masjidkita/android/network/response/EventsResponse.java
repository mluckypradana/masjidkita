package org.masjidkita.android.network.response;

import org.masjidkita.android.database.models.Event;

import java.util.List;

/**
 * Created by MuhammadLucky on 26-Oct-15.
 */
public class EventsResponse extends BaseResponse {
    private List<Event> data;

    public List<Event> getData() {
        return data;
    }
}
