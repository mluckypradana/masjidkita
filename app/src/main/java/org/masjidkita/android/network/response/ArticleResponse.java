package org.masjidkita.android.network.response;

import org.masjidkita.android.database.models.Article;

import java.util.List;

/**
 * Created by MuhammadLucky on 26-Oct-15.
 */
public class ArticleResponse extends BaseResponse {
    private Article data;

    public Article getData() {
        return data;
    }
}
