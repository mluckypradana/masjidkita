package org.masjidkita.android.network.response;

import org.masjidkita.android.database.models.Article;
import org.masjidkita.android.database.models.Module;

import java.util.List;

/**
 * Created by MuhammadLucky on 26-Oct-15.
 */
public class ModulesResponse extends BaseResponse {
    private List<Module> data;

    public List<Module> getData() {
        return data;
    }
}
