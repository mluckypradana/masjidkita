package org.masjidkita.android.network.response;

import org.masjidkita.android.database.models.Mosque;

import java.util.List;

/**
 * Created by MuhammadLucky on 26-Oct-15.
 */
public class MosquesResponse extends BaseResponse {
    public List<Mosque> data;

    public List<Mosque> getData() {
        return data;
    }

}
