package org.masjidkita.android.network.response;

import org.masjidkita.android.database.models.Article;
import org.masjidkita.android.database.models.Event;

/**
 * Created by MuhammadLucky on 26-Oct-15.
 */
public class EventResponse extends BaseResponse {
    private Event data;

    public Event getData() {
        return data;
    }
}
