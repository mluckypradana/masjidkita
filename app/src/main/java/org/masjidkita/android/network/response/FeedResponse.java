package org.masjidkita.android.network.response;

import org.masjidkita.android.database.models.Feed;

import java.util.List;

/**
 * Created by MuhammadLucky on 26-Oct-15.
 */
public class FeedResponse extends BaseResponse {
    private List<Feed> data;

    public List<Feed> getData() {
        return data;
    }
}
