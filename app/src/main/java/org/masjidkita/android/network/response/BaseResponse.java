package org.masjidkita.android.network.response;

/**
 * Created by MuhammadLucky on 26-Oct-15.
 */
public class BaseResponse {
    public static final int STATUS_FAILURE = 0;
    public static final int STATUS_SUCCESS = 1;
    private Integer status;
    private String message;

    public Integer getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
