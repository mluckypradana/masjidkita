package org.masjidkita.android.network.response;

import org.masjidkita.android.database.models.User;

/**
 * Created by MuhammadLucky on 26-Oct-15.
 */
public class LoginResponse extends BaseResponse {
    User data;

    public User getUser() {
        return data;
    }

}
