package org.masjidkita.android.network.request;

/**
 *
 * Created by MuhammadLucky on 26-Oct-15.
 */
public class LoginRequest extends BaseRequest{
    private String email;
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
