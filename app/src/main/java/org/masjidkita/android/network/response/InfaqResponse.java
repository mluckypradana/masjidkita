package org.masjidkita.android.network.response;

import org.masjidkita.android.database.models.Article;
import org.masjidkita.android.database.models.Infaq;

/**
 * Created by MuhammadLucky on 26-Oct-15.
 */
public class InfaqResponse extends BaseResponse {
    private Infaq data;

    public Infaq getData() {
        return data;
    }
}
