package org.masjidkita.android.listeners;

/**
 * Created by MuhammadLucky on 02/09/2016.
 * For feed adapters at Home menu
 */
public interface OnItemClickListener {
    void onMosqueClicked(int position);

    void onItemClicked(int position);
}