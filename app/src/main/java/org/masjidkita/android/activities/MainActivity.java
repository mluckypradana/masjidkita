package org.masjidkita.android.activities;

import android.Manifest;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.masjidkita.android.R;
import org.masjidkita.android.fragments.AboutFragment;
import org.masjidkita.android.fragments.AboutMtwFragment;
import org.masjidkita.android.fragments.AllMosqueFragment;
import org.masjidkita.android.fragments.ArticleFragment;
import org.masjidkita.android.fragments.CoreFragment;
import org.masjidkita.android.fragments.EventFragment;
import org.masjidkita.android.fragments.FavoritedMosqueFragment;
import org.masjidkita.android.fragments.HomeFragment;
import org.masjidkita.android.fragments.InfaqFragment;
import org.masjidkita.android.fragments.ModuleFragment;
import org.masjidkita.android.fragments.NearestMosqueFragment;
import org.masjidkita.android.fragments.NotificationFragment;
import org.masjidkita.android.fragments.ProfileFragment;
import org.masjidkita.android.fragments.RecommendationFragment;
import org.masjidkita.android.fragments.ScheduleFragment;
import org.masjidkita.android.fragments.SettingFragment;
import org.masjidkita.android.fragments.TrainingMtwFragment;
import org.masjidkita.android.fragments.VipFragment;
import org.masjidkita.android.fragments.WalletFragment;
import org.masjidkita.android.helpers.SessionHelper;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends CoreActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final int PERMISSION_LOCATION_REQUEST = 843;

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    private HomeFragment mHomeFragment;
    private FavoritedMosqueFragment mFavoritedMosqueFragment;
    private ArticleFragment articleFragment;
    private EventFragment eventFragment;
    private InfaqFragment infaqFragment;
    private ModuleFragment moduleFragment;
    private AllMosqueFragment allMosqueFragment;
    private NotificationFragment notificationFragment;
    private WalletFragment walletFragment;
    private ScheduleFragment scheduleFragment;
    private AboutMtwFragment aboutMtwFragment;
    private TrainingMtwFragment trainingMtwFragment;
    private SettingFragment settingFragment;
    private ProfileFragment profileFragment;
    private AboutFragment aboutFragment;
    private VipFragment vipFragment;
    private NearestMosqueFragment nearestMosqueFragment;

    //Location variables
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private RecommendationFragment recommendationFragment;

    /**
     * Add fragment to this activity fragment handling.
     *
     * @param fragment
     */
    public void addFragment(CoreFragment fragment) {
        removeFragment(fragment);
        addFragment(R.id.ll_main, fragment);
        closeDrawer();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_main);
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        createLocationRequest();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        showHomePage();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
        else {
//            super.onBackPressed();
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the FeedAdapter/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

//        switch (id) {
//            case R.id.nav_home:
//                showHomePage();
//                break;
//            case R.id.nav_all_mosque:
//                showAllMosque();
//                break;
//            case R.id.nav_favorited_mosque:
//                showFavoritedMosques();
//                break;
//            case R.id.nav_near_mosque:
//                showNearMosques();
//                break;
//            case R.id.nav_logout:
//                logout();
//                break;
//        }

        closeDrawer();
        return true;
    }

    private void closeDrawer() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    @OnClick(R.id.nav_logout)
    void logout() {
        //Clear firebase session
        final FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser user = firebaseAuth.getCurrentUser();
        firebaseAuth.signOut();
        if (user != null)
            user.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    showLoginPage();
                }
            });
        else
            showLoginPage();
    }

    private void showLoginPage() {
        getSession().put(SessionHelper.USER_ID, 0);
        String token = null;
        getSession().put(SessionHelper.TOKEN, token);

        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition();
        finish();
    }

    /**
     * Add NewMosqueFragment
     */
    private void showNearMosques() {
//        if (mNearMosquesFragment == null)
//            mNearMosquesFragment = new NearMosquesFragment();
//        addFragment(mNearMosquesFragment);
    }

    /**
     * Add HomeFragment to this activity
     */
    @OnClick(R.id.nav_home)
    void showHomePage() {
//        if (mHomeFragment == null)
        mHomeFragment = new HomeFragment();
        addFragment(mHomeFragment);
    }

    /**
     * Masjid mosque
     */
    @OnClick(R.id.nav_favorited_mosque)
    void showFavoritedMosques() {
        if (mFavoritedMosqueFragment == null)
            mFavoritedMosqueFragment = new FavoritedMosqueFragment();
        addFragment(mFavoritedMosqueFragment);
    }

    /**
     * Start AllMosqueFragment
     */
    @OnClick(R.id.nav_all_mosque)
    void showAllMosque() {
        if (allMosqueFragment == null)
            allMosqueFragment = new AllMosqueFragment();
        addFragment(allMosqueFragment);
    }

    /**
     * Start AllMosqueFragment
     */
    @OnClick(R.id.nav_nearest_mosque)
    void showNearestMosque() {
        if (nearestMosqueFragment == null)
            nearestMosqueFragment = new NearestMosqueFragment();
        addFragment(nearestMosqueFragment);
    }

    @OnClick(R.id.nav_recommendation)
    void showRecommendationPage(){
        closeDrawer();
//        if(recommendationFragment==null)
//            recommendationFragment = new RecommendationFragment();
//        addFragment(recommendationFragment);
        Intent intent = new Intent(this, RecommendationActivity.class);
        startActivity(intent);
    }

    /**
     * Start AllMosqueFragment
     */
    @OnClick(R.id.nav_notification)
    void showNotification() {
        if (notificationFragment == null)
            notificationFragment = new NotificationFragment();
        addFragment(notificationFragment);
    }

    /**
     * Start Article Fragment
     */
    @OnClick(R.id.nav_article)
    void showMyArticles() {
        if (articleFragment == null) {
            articleFragment = new ArticleFragment();
            articleFragment.setMine(true);
        }
        addFragment(articleFragment);
    }

    /**
     * Start Event Fragment
     */
    @OnClick(R.id.nav_event)
    void showMyEvents() {
        if (eventFragment == null) {
            eventFragment = new EventFragment();
            eventFragment.setMine(true);
        }
        addFragment(eventFragment);
    }

    /**
     * Start Infaq Fragment
     */
    @OnClick(R.id.nav_infaq)
    void showMyInfaqs() {
        if (infaqFragment == null) {
            infaqFragment = new InfaqFragment();
            infaqFragment.setMine(true);
        }
        addFragment(infaqFragment);
    }

    /**
     * Start Module Fragment
     */
    @OnClick(R.id.nav_my_module)
    void showMyModule() {
//        if (moduleFragment == null)
        moduleFragment = new ModuleFragment();
        addFragment(moduleFragment);
    }

    /**
     * Start Module Fragment
     */
    @OnClick(R.id.nav_wallet)
    void showMyWallet() {
//        if (moduleFragment == null)
        walletFragment = new WalletFragment();
        addFragment(walletFragment);
    }

    /**
     * Start Schedule Fragment
     */
    @OnClick(R.id.nav_schedule)
    void showScheduleFragment() {
//        if (moduleFragment == null)
        scheduleFragment = new ScheduleFragment();
        addFragment(scheduleFragment);
    }

    /**
     * Start compass fragment
     */
    @OnClick(R.id.nav_compass)
    void showMakkahCompass() {
        closeDrawer();
        Intent intent = new Intent(this, CompassActivity.class);
        startActivity(intent);
    }

    /**
     * Start Abouts MTW Fragment
     */
    @OnClick(R.id.nav_about_mtw)
    void showMtwFragment() {
//        if (moduleFragment == null)
        aboutMtwFragment = new AboutMtwFragment();
        addFragment(aboutMtwFragment);
    }


    /**
     * Start Abouts MTW Fragment
     */
    @OnClick(R.id.nav_training)
    void showTrainingMtwFragment() {
//        if (moduleFragment == null)
        trainingMtwFragment = new TrainingMtwFragment();
        addFragment(trainingMtwFragment);
    }

    /**
     * Start Setting Fragment
     */
    @OnClick(R.id.nav_setting)
    void showSetting() {
//        if (moduleFragment == null)
        settingFragment = new SettingFragment();
        addFragment(settingFragment);
    }

    /**
     * Start Profile Fragment
     */
    @OnClick(R.id.nav_profile)
    void showProfile() {
//        if (moduleFragment == null)
        profileFragment = new ProfileFragment();
        addFragment(profileFragment);
    }

    /**
     * Start About Fragment
     */
    @OnClick(R.id.nav_about)
    void showAbout() {
//        if (moduleFragment == null)
        aboutFragment = new AboutFragment();
        addFragment(aboutFragment);
    }

    /**
     * Start About Fragment
     */
    @OnClick(R.id.nav_vip)
    void showVIPPage() {
//        if (moduleFragment == null)
        vipFragment = new VipFragment();
        addFragment(vipFragment);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onLocationChanged(Location location) {
        getSession().put(SessionHelper.LATITUDE, location.getLatitude() + "");
        getSession().put(SessionHelper.LONGITUDE, location.getLongitude() + "");
//        this.currentLocation = location;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    /**
     * Get longitude and lattitude
     */
    public void createLocationRequest() {
//        if (googleApiClient == null) {
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
//        }
        googleApiClient.connect();

        //Init locationRequest
        locationRequest = new LocationRequest();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        // **************************
        builder.setAlwaysShow(true); // this is the key ingredient
        // **************************

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                .checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result
                        .getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All currentLocation settings are satisfied. The client can
                        // initialize currentLocation
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be
                        // fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling
                            // startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(MainActivity.this, 1000);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have
                        // no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    /**
     * Request currentLocation update, need check permission
     */
    protected void startLocationUpdates() {
        //Permission for currentLocation
        String[] permissions = new String[]{
                Manifest.permission.INTERNET,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
        };
        //Show runtime permission
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, permissions, PERMISSION_LOCATION_REQUEST);
            return;
        }
        //Show denied permission
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_DENIED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_DENIED) {
            Toast.makeText(this, "Unnable to request permission", Toast.LENGTH_SHORT);
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
//        mRequestingLocationUpdates = true;
    }
}