package org.masjidkita.android.activities;

import android.content.Context;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import org.masjidkita.android.Config;
import org.masjidkita.android.MyApplication;
import org.masjidkita.android.database.DatabaseHelper;
import org.masjidkita.android.helpers.FontHelper;
import org.masjidkita.android.helpers.SessionHelper;
import org.masjidkita.android.network.ApiService;

/**
 * A login screen that offers login via email/password.
 */
public class CoreActivity extends AppCompatActivity {
    //View animator layout index
    public static final int LAYOUT_MAIN = 0;
    public static final int LAYOUT_PROGRESS = 1;
    public static final int LAYOUT_NO_DATA = 2;
    public static final int LAYOUT_FAILED = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Set default font
        FontHelper.setDefaultFont(this, "DEFAULT", Config.Font.DEFAULT);
        FontHelper.setDefaultFont(this, "MONOSPACE", Config.Font.DEFAULT);
        FontHelper.setDefaultFont(this, "SERIF", Config.Font.BOLD);

        super.onCreate(savedInstanceState);
    }

    /**
     * Add fragment with transition
     */
    public void addFragment(final int frameId, final Fragment fragment) {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();

                //Set custom animation before replace
                transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out);

                transaction.replace(frameId, fragment);
                Fragment existFragment = getSupportFragmentManager().findFragmentById(frameId);
                if (existFragment != null)
                    transaction.addToBackStack(null);
                transaction.commitAllowingStateLoss();
            }
        });
    }

    /**
     * Customize font to specific font face.
     *
     * @param textViews As {@link TextView} component
     */
    protected void customizeFonts(String fontFace, TextView... textViews) {
        Typeface typeFace = Typeface.createFromAsset(getAssets(), fontFace);
        for(TextView textView:textViews) {
            textView.setTypeface(typeFace);
            textView.invalidate();
        }
    }

    /**
     * Remove fragment from this activity
     *
     * @param fragment
     */
    public void removeFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.remove(fragment);
        transaction.commit();
    }

    /**
     * Get API Service from Application
     */
    public ApiService getService() {
        return ((MyApplication) getApplication()).getService();
    }

    /**
     * Get Shared Preferences
     */
    public SessionHelper getSession() {
        return ((MyApplication) getApplication()).getSession();
    }


    /**
     * Get ORMLite DatabaseHelper
     */
    public DatabaseHelper getDatabase() {
        return ((MyApplication) getApplication()).getDatabase();
    }

    protected boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null;
    }

    /**
     * Add animation transition to activity
     */
    protected void overridePendingTransition() {
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    protected void showToast(int resId) {
        Toast.makeText(this, resId, Toast.LENGTH_SHORT).show();
    }

    protected void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}