package org.masjidkita.android.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ViewAnimator;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import org.masjidkita.android.Config;
import org.masjidkita.android.R;
import org.masjidkita.android.helpers.LayoutHelper;
import org.masjidkita.android.helpers.SessionHelper;
import org.masjidkita.android.network.response.BaseResponse;
import org.masjidkita.android.network.response.RegisterResponse;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends CoreActivity implements Validator.ValidationListener {
    @Email(messageResId = R.string.error_email_invalid)
    @Bind(R.id.et_email)
    EditText etEmail;
    @NotEmpty(messageResId = R.string.error_password_empty)
    @Password(min = 6, messageResId = R.string.error_password_length)
    @Bind(R.id.et_password)
    EditText etPassword;
    @NotEmpty(messageResId = R.string.error_password_empty)
    @ConfirmPassword(messageResId = R.string.error_confirm_password_doesnt_match)
    @Bind(R.id.et_confirm_password)
    EditText etConfirmPassword;
    @Bind(R.id.va_main)
    ViewAnimator vaMain;
    private Validator validator;
    private boolean registering;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle(R.string.title_register);
//        getSupportActionBar().setLo
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
//        setHasOptionsMenu(true);
        initValidator();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.i_register:
                if (!registering) {
                    registering = true;

                    LayoutHelper.hideKeyboard(this);
                    //Validate, coninue to listeners
                    validator.validate();
                }
                break;
        }

        return true;
    }

    /**
     * Validator using saripaar
     */
    private void initValidator() {
        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    /**
     * Validate first
     */
    private void register() {
        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();
        String confirmPassword = etConfirmPassword.getText().toString();

        //Register to API
        showLoading();
        Call<RegisterResponse> call = getService().register(email, password, confirmPassword);
        call.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                BaseResponse body = response.body();
                if (response.code() == 200
                        && response.body().getStatus() == BaseResponse.STATUS_SUCCESS) {
                    saveSession();
                    backToLoginPage(response.body().getMessage());
                } else
                    showError(body != null ? body.getMessage() : getResources().getString(R.string.error_register));
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                showError(getResources().getString(R.string.error_register));
            }
        });

    }

    private void showError(String resId) {
        registering = false;
        if (vaMain == null)
            return;

        showToast(resId);
        vaMain.setDisplayedChild(LAYOUT_MAIN);
    }

    private void showLoading() {
        vaMain.setDisplayedChild(LAYOUT_PROGRESS);
    }

    /**
     * Save credential to shared preferences
     */
    private void saveSession() {
        getSession().put(SessionHelper.USER_ID, 55);
        getSession().put(SessionHelper.TOKEN, getString(R.string.default_token));


    }

    /**
     * Show login page with extra dialog
     */
    private void backToLoginPage(String message) {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra(Config.Extra.MESSAGE, message);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        overridePendingTransition();
        finish();
    }

    @Override
    public void onValidationSucceeded() {
        register();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        registering = false;

        for (ValidationError error : errors) {
            //Show error to every editText
            EditText editText = (EditText) error.getView();
            editText.setError(error.getCollatedErrorMessage(this));
        }
    }
}
