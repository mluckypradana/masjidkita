package org.masjidkita.android.activities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ViewAnimator;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;

import org.masjidkita.android.Config;
import org.masjidkita.android.R;
import org.masjidkita.android.database.models.Mosque;
import org.masjidkita.android.helpers.SessionHelper;
import org.masjidkita.android.network.response.BaseResponse;
import org.masjidkita.android.network.response.MosquesResponse;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecommendationActivity extends CoreActivity implements
        OnMapReadyCallback,
        Validator.ValidationListener {

    @Bind(R.id.ll_form)
    LinearLayout llForm;
    @Bind(R.id.va_main)
    ViewAnimator vaMain;
    @Bind(R.id.et_mosque_name)
    EditText etMosqueName;
    @Bind(R.id.et_address)
    EditText etAddress;
    @Bind(R.id.et_name)
    EditText etName;
    @Bind(R.id.et_mobile)
    EditText etMobile;
    @Bind(R.id.ll_failed_fetch)
    LinearLayout llFailedFetch;

    private GoogleMap mMap;
    private List<Mosque> mosques = new ArrayList<>();
    private Dao<Mosque, Integer> mosqueDao;
    private boolean loading;
    private MenuItem mAdd;
    private boolean addingRecommendation;
    private Validator validator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recommendation);
        ButterKnife.bind(this);

//        vaMain = (ViewAnimator) findViewById(R.id.va_main);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        setupDao();
        loadData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.recommendation, menu);
        mAdd = menu.findItem(R.id.m_add);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.m_add:
                //If recommendation form not yet showed, show form
                if (!addingRecommendation)
                    llForm.setVisibility(View.VISIBLE);
                    //If recommendation form showed, validate recommedation form
                else
                    validator.validate();
                break;
        }
        return true;
    }

    @OnClick(R.id.ll_failed_fetch)
    void tryFetch() {
        loadServerData(0);
    }

    private void setupDao() {
        try {
            mosqueDao = getDatabase().getDao(Mosque.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Load data from server / local
     */
    private void loadData() {
        loadLocalData();
        if (isNetworkConnected())
            loadServerData(0);
    }

    /**
     * Load data from  local
     */
    private void loadLocalData() {
        try {
            showLoading();
            mosques.clear();

            QueryBuilder<Mosque, Integer> builder = mosqueDao.queryBuilder();
            builder.where().le(Mosque.DISTANCE, Config.MAX_DISTANCE);
            mosques.addAll(builder.query());
            showCompletedData();
        } catch (SQLException e) {
            showError();
            e.printStackTrace();
        }
    }

    /**
     * Load data from server if connected, if offset 0 = load first data
     */
    private void loadServerData(final int offset) {
        loading = true;

        int userId = getSession().getInt(SessionHelper.USER_ID);
        String token = getSession().getString(SessionHelper.TOKEN);
        String latitude = getSession().getString(SessionHelper.LATITUDE);
        String longitude = getSession().getString(SessionHelper.LONGITUDE);

        showLoading();

        //If from article menu
        Call<MosquesResponse> call = getService().getMosquesRecommendation(userId, token, latitude, longitude, Config.MAX_DISTANCE);
        call.enqueue(new Callback<MosquesResponse>() {
            @Override
            public void onResponse(Call<MosquesResponse> call, Response<MosquesResponse> response) {
                onLoadDataCompleted(response, offset);
            }

            @Override
            public void onFailure(Call<MosquesResponse> call, Throwable t) {
                showError();
            }
        });

    }

    /**
     * On load completed after request data
     */
    private void onLoadDataCompleted(Response<MosquesResponse> response, int offset) {
        if (response.code() == 200) {

            //Populate to mosques list
            mosques.clear();
            mosques.addAll(response.body().getData());

            // Add a marker in Specific and move the camera
            String latitude = getSession().getString(SessionHelper.LATITUDE);
            String longitude = getSession().getString(SessionHelper.LONGITUDE);
//        LatLng sydney = new LatLng(-34, 151);
            LatLng myCurrentLocation = new LatLng(Double.valueOf(latitude), Double.valueOf(longitude));
            mMap.addMarker(new MarkerOptions()
                    .position(myCurrentLocation)
                    .title("Lokasi anda"));

            //Populate mosques to map
            Bitmap mosqueIcon = getMosqueIcon(60, 90);
            for (Mosque mosque : mosques) {
                LatLng location = new LatLng(Double.valueOf(mosque.getLatitude()), Double.valueOf(mosque.getLongitude()));
                mMap.addMarker(new MarkerOptions()
                        .position(location)
                        .title(mosque.getName())
                        .icon(BitmapDescriptorFactory.fromBitmap(mosqueIcon)));
            }

            mMap.moveCamera(CameraUpdateFactory.newLatLng(myCurrentLocation));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(17.0f));

            showCompletedData();
        } else
            showError();
    }

    public Bitmap getMosqueIcon(int width, int height) {
        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(),
                R.mipmap.ic_mosque_location);
        return Bitmap.createScaledBitmap(imageBitmap, width, height, false);
    }

    /**
     * Show load progress if datalist equals 0
     */
    private void showLoading() {
        if (mosques.size() == 0)
            vaMain.setDisplayedChild(LAYOUT_PROGRESS);
    }

    /**
     * Set view animator visibility if necessary
     */
    private void showCompletedData() {
        if (mosques.size() > 0)
            vaMain.setDisplayedChild(LAYOUT_MAIN);
        else
            vaMain.setDisplayedChild(LAYOUT_NO_DATA);
//        vaMain.setDisplayedChild(mosques.size() > 0 ? LAYOUT_MAIN : LAYOUT_NO_DATA);
    }

    /**
     * Show error page
     */
    private void showError() {
        if (mosques.size() == 0)
            vaMain.setDisplayedChild(LAYOUT_FAILED);
        else
            showToast(R.string.message_failed_fetch);
    }

    /**
     * Validator using saripaar
     */
    private void initValidator() {
        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    /**
     * Call recommendation API
     */
    private void sendData() {
        int userId = getSession().getInt(SessionHelper.USER_ID);
        String token = getSession().getString(SessionHelper.TOKEN);
        String latitude = getSession().getString(SessionHelper.LATITUDE);
        String longitude = getSession().getString(SessionHelper.LONGITUDE);
        String address = etAddress.getText().toString();
        String mosqueName = etMosqueName.getText().toString();
        String mobile = etMobile.getText().toString();
        String caretakerName = etName.getText().toString();

        Call<BaseResponse> call = getService().postMosqueRecommendation(
                userId,
                token,
                latitude,
                longitude,
                address,
                mosqueName,
                mobile,
                caretakerName);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseResponse body = response.body();
                if (response.code() == 200 && body.getStatus() == BaseResponse.STATUS_SUCCESS) {
                    showCompletedData();
                    //TODO Reshow maps activity
                } else
                    showToast(body != null ? body.getMessage() : getResources().getString(R.string.error_recommend));
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                showToast(getResources().getString(R.string.error_recommend));
            }
        });
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }

    @Override
    public void onValidationSucceeded() {
        sendData();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            //Show error to every editText
            EditText editText = (EditText) error.getView();
            editText.setError(error.getCollatedErrorMessage(this));
        }
    }
}
