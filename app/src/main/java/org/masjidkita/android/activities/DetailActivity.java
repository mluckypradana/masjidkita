package org.masjidkita.android.activities;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.ViewAnimator;

import com.bumptech.glide.Glide;
import com.github.florent37.glidepalette.GlidePalette;
import com.j256.ormlite.dao.Dao;

import org.masjidkita.android.Config;
import org.masjidkita.android.R;
import org.masjidkita.android.database.models.Article;
import org.masjidkita.android.database.models.Event;
import org.masjidkita.android.database.models.Feed;
import org.masjidkita.android.database.models.Infaq;
import org.masjidkita.android.database.models.Module;
import org.masjidkita.android.helpers.CurrencyHelper;
import org.masjidkita.android.helpers.DateHelper;
import org.masjidkita.android.helpers.HtmlHelper;
import org.masjidkita.android.helpers.SessionHelper;
import org.masjidkita.android.network.response.ArticleResponse;
import org.masjidkita.android.network.response.EventResponse;
import org.masjidkita.android.network.response.InfaqResponse;
import org.masjidkita.android.network.response.ModuleResponse;

import java.sql.SQLException;
import java.text.MessageFormat;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailActivity extends CoreActivity {
    @Bind(R.id.va_main)
    ViewAnimator vaMain;
    @Bind(R.id.iv_thumb)
    ImageView ivThumb;
    @Bind(R.id.tv_content)
    TextView tvContent;
    @Bind(R.id.sv_main)
    ScrollView svMain;
    @Bind(R.id.fab_action_1)
    FloatingActionButton fabAction1;
    @Bind(R.id.fab_action_2)
    FloatingActionButton fabAction2;
    @Bind(R.id.t_title)
    TextView tvTitle;
    @Bind(R.id.tv_date)
    TextView tvDate;
    @Bind(R.id.tv_read_total)
    TextView tvReadTotal;
    @Bind(R.id.cl_main)
    CoordinatorLayout clMain;
    @Bind(R.id.tv_mosque)
    TextView tvMosque;
    @Bind(R.id.tv_person)
    TextView tvPerson;
    @Bind(R.id.tv_label_1)
    TextView tvLabel1;
    @Bind(R.id.tv_detail_1)
    TextView tvDetail1;
    @Bind(R.id.tv_label_2)
    TextView tvLabel2;
    @Bind(R.id.tv_detail_2)
    TextView tvDetail2;
    @Bind(R.id.ll_detail)
    LinearLayout llDetail;

    private Article article;
    private Event event;
    private Infaq infaq;
    private Module module;
    private String type;
    private Dao<Article, Integer> articleDao;
    private Dao<Event, Integer> eventDao;
    private Dao<Infaq, Integer> infaqDao;
    private Dao<Module, Integer> moduleDao;
    private int id = 0;
    private Dao<Feed, Integer> feedDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        hideActionbar();

//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);


        customizeFonts();
        getExtras();
        setupDao();
        loadData();
        setupFab();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    private void hideActionbar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
    }

    private void customizeFonts() {
        customizeFonts(Config.Font.BOLD, tvMosque, tvDate, tvReadTotal);
        customizeFonts(Config.Font.LIGHT, tvTitle);
        customizeFonts(Config.Font.DEFAULT, tvContent);
    }

    /**
     * Get Intent extra from main activity
     */
    private void getExtras() {
        type = getIntent().getStringExtra(Config.Extra.TYPE);
        id = getIntent().getIntExtra(Config.Extra.ID, 0);
    }

    /**
     * Setup fab button for every type of object
     */
    private void setupFab() {
        if (type.equals(Feed.TYPE_ARTICLE)) {
            fabAction1.setImageResource(R.mipmap.ic_share_inverse);
            fabAction2.setVisibility(View.GONE);
        }
        if (type.equals(Feed.TYPE_EVENT)) {
            fabAction1.setImageResource(R.mipmap.ic_register_inverse);
            fabAction2.setImageResource(R.mipmap.ic_alarm_add_inverse);
        }
        if (type.equals(Feed.TYPE_INFAQ)) {
            fabAction1.setImageResource(R.mipmap.ic_share_inverse);
            fabAction2.setImageResource(R.mipmap.ic_coin_inverse);
        }
        if (type.equals(Feed.TYPE_MODULE)) {
            fabAction1.setVisibility(View.GONE);
            fabAction2.setVisibility(View.GONE);
        }
    }

    /**
     * Setup every dao in database helper
     */
    private void setupDao() {
        try {
            feedDao = getDatabase().getDao(Feed.class);
            articleDao = getDatabase().getDao(Article.class);
            eventDao = getDatabase().getDao(Event.class);
            infaqDao = getDatabase().getDao(Infaq.class);
            moduleDao = getDatabase().getDao(Module.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Load data from server / local
     */
    private void loadData() {
        loadLocalData();
        if (isNetworkConnected())
            loadServerData(id);
    }

    /**
     * Load data from  local
     */
    private void loadLocalData() {
        try {
            showLoading();
            Feed feed = feedDao.queryForId(id);
            if (type.equals(Feed.TYPE_ARTICLE)) {
                Article tempArticle = articleDao.queryForId(id);
                if (tempArticle != null)
                    article = tempArticle;
                else {
                    //Create data from feed
                    article = new Article(feed);
                    articleDao.createOrUpdate(article);
                }
            } else if (type.equals(Feed.TYPE_EVENT)) {
                Event tempData = eventDao.queryForId(id);
                if (tempData != null)
                    event = tempData;
                else {
                    //Create data from feed
                    event = new Event(feed);
                    eventDao.createOrUpdate(event);
                }
            } else if (type.equals(Feed.TYPE_INFAQ)) {
                Infaq tempData = infaqDao.queryForId(id);
                if (tempData != null)
                    infaq = tempData;
                else {
                    //Create data from feed
                    infaq = new Infaq(feed);
                    infaqDao.createOrUpdate(infaq);
                }
            } else if (type.equals(Feed.TYPE_MODULE)) {
                Module tempData = moduleDao.queryForId(id);
                if (tempData != null)
                    module = tempData;
                else {
                    //Create data from feed
                    module = new Module(feed);
                    moduleDao.createOrUpdate(module);
                }
            }
            showCompletedData();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Load data from server if connected, if offset 0 = load first data
     */
    private void loadServerData(Integer id) {
        int userId = getSession().getInt(SessionHelper.USER_ID);
        String token = getSession().getString(SessionHelper.TOKEN);

        showLoading();
        if (type.equals(Feed.TYPE_ARTICLE)) {
            Call<ArticleResponse> call = getService().getArticle(id, userId, token);
            call.enqueue(new Callback<ArticleResponse>() {
                @Override
                public void onResponse(Call<ArticleResponse> call, Response<ArticleResponse> response) {
                    if (response.code() == 200) {
                        ArticleResponse articleResponse = response.body();

                        //Save to database (excerpt is null)
                        if (article != null)
                            articleResponse.getData().setExcerpt(article.getExcerpt());
                        article = articleResponse.getData();
                        try {
                            articleDao.createOrUpdate(article);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        article = articleResponse.getData();

                        showCompletedData();
                    } else {
                        showToast(R.string.message_failed_fetch);
                        showError();
                    }
                }

                @Override
                public void onFailure(Call<ArticleResponse> call, Throwable t) {
                    showError();
                }
            });
        } else if (type.equals(Feed.TYPE_EVENT)) {
            Call<EventResponse> call = getService().getEvent(id, userId, token);
            call.enqueue(new Callback<EventResponse>() {
                @Override
                public void onResponse(Call<EventResponse> call, Response<EventResponse> response) {
                    if (response.code() == 200) {
                        EventResponse eventResponse = response.body();

                        //Save to database
                        try {
                            eventDao.createOrUpdate(eventResponse.getData());
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }

                        showCompletedData();
                    } else {
                        showToast(R.string.message_failed_fetch);
                        showError();
                    }
                }

                @Override
                public void onFailure(Call<EventResponse> call, Throwable t) {
                    showError();
                }
            });
        } else if (type.equals(Feed.TYPE_INFAQ)) {
            Call<InfaqResponse> call = getService().getInfaq(id, userId, token);
            call.enqueue(new Callback<InfaqResponse>() {
                @Override
                public void onResponse(Call<InfaqResponse> call, Response<InfaqResponse> response) {
                    if (response.code() == 200) {
                        InfaqResponse infaqResponse = response.body();

                        //Save to database
                        try {
                            infaqDao.createOrUpdate(infaqResponse.getData());
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }

                        showCompletedData();
                    } else {
                        showToast(R.string.message_failed_fetch);
                        showError();
                    }
                }

                @Override
                public void onFailure(Call<InfaqResponse> call, Throwable t) {
                    showError();
                }
            });
        } else if (type.equals(Feed.TYPE_MODULE)) {
            int seed = 0;
            Call<ModuleResponse> call = getService().getModule(id, userId, token, seed);
            call.enqueue(new Callback<ModuleResponse>() {
                @Override
                public void onResponse(Call<ModuleResponse> call, Response<ModuleResponse> response) {
                    if (response.code() == 200) {
                        ModuleResponse moduleResponse = response.body();

                        //Save to database
                        try {
                            moduleDao.createOrUpdate(moduleResponse.getData());
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }

                        showCompletedData();
                    } else {
                        showToast(R.string.message_failed_fetch);
                        showError();
                    }
                }

                @Override
                public void onFailure(Call<ModuleResponse> call, Throwable t) {
                    showError();
                }
            });
        }
    }

    /**
     * Show load progress if datalist equals 0
     */
    private void showLoading() {
        vaMain.setDisplayedChild(LAYOUT_PROGRESS);
    }

    /**
     * Set view animator visibility if necessary
     */
    private void showCompletedData() {
        if (vaMain == null)
            return;
        vaMain.setDisplayedChild(LAYOUT_MAIN);

        if (type.equals(Feed.TYPE_ARTICLE))
            showArticle();
        if (type.equals(Feed.TYPE_EVENT))
            showEvent();
        if (type.equals(Feed.TYPE_INFAQ))
            showInfaq();
        if (type.equals(Feed.TYPE_MODULE))
            showModule();
    }

    private void showModule() {
        if (module == null)
            return;
        String thumbnail = module.getThumbnail();
        String title = module.getTitle();
        String hit = getResources().getString(R.string.label_module_hit) + " " + MessageFormat.format(
                getResources().getString(R.string.label_read_times),
                module.getHit());
        String category = module.getCategory();
        String content = module.getDescription();

        tvTitle.setText(title);
        tvDate.setText(hit);
        tvPerson.setText(R.string.label_module_category);
        tvMosque.setText(category);
        llDetail.setVisibility(View.GONE);
        tvLabel1.setText(R.string.label_infaq_collected);
        tvLabel2.setText(R.string.label_infaq_target);
        if (content != null)
            tvContent.setText(HtmlHelper.fromHtml(content));
        else
            tvContent.setVisibility(View.GONE);

        Glide.with(this)
                .load(thumbnail)
                .listener(GlidePalette.with(thumbnail)
                        .use(Config.GLIDE_PALETTE_PROFILE)
                        .intoBackground(ivThumb)
                        .crossfade(true)
                )
                .into(ivThumb);
    }

    private void showInfaq() {
        if (infaq == null)
            return;
        String thumbnail = infaq.getThumbnail();
        String title = infaq.getName();
        String date = DateHelper.parseDate(infaq.getDate());
        String content = infaq.getDescription();
        if (content == null)
            content = infaq.getExcerpt();
        String mosque = infaq.getMasjid();
        String detail1 = "Rp. " + CurrencyHelper.parseCurrency(infaq.getNominal());
        String detail2 = "Rp. " + CurrencyHelper.parseCurrency(infaq.getTarget());

        tvTitle.setText(title);
        tvDate.setText(date);
        tvMosque.setText(mosque);
        tvReadTotal.setVisibility(View.GONE);

        llDetail.setVisibility(View.VISIBLE);
        if (detail1 != null) {
            tvLabel1.setText(R.string.label_infaq_collected);
            tvDetail1.setText(detail1);
        } else {
            tvLabel1.setVisibility(View.GONE);
            tvDetail1.setVisibility(View.GONE);
        }
        if (detail2 != null) {
            tvLabel2.setText(R.string.label_infaq_target);
            tvDetail2.setText(detail2);
        } else {
            tvLabel1.setVisibility(View.GONE);
            tvDetail1.setVisibility(View.GONE);
        }

        if (content != null)
            tvContent.setText(HtmlHelper.fromHtml(content));

        Glide.with(this)
                .load(thumbnail)
                .listener(GlidePalette.with(thumbnail)
                        .use(Config.GLIDE_PALETTE_PROFILE)
                        .intoBackground(ivThumb)
                        .crossfade(true)
                )
                .into(ivThumb);
    }


    private void showEvent() {
        if (event == null)
            return;
        String title = event.getName();
        String date = "";
        if (event.getEventDetail() != null)
            date = DateHelper.parseDate(event.getEventDetail().getDate().split(" ")[0]);
        String content = event.getDescription();
        if (content == null)
            content = event.getExcerpt();
        String mosque = event.getMasjid();
        String detail1 = event.getUstadz();
        String detail2 = event.getTime();

        tvTitle.setText(title);
        tvDate.setText(date);
        tvMosque.setText(mosque);
        tvReadTotal.setVisibility(View.GONE);

        llDetail.setVisibility(View.VISIBLE);
        if (detail1 != null) {
            tvLabel1.setText(R.string.label_event_ustadz);
            tvDetail1.setText(detail1);
        } else {
            tvLabel1.setVisibility(View.GONE);
            tvDetail1.setVisibility(View.GONE);
        }
        if (detail2 != null) {
            tvLabel2.setText(R.string.label_event_hour);
            tvDetail2.setText(detail2);
        } else {
            tvLabel2.setVisibility(View.GONE);
            tvDetail2.setVisibility(View.GONE);
        }

        if (content != null)
            tvContent.setText(HtmlHelper.fromHtml(content));
//        Glide.with(this)
//                .load(article.getThumbnail())
//                .listener(GlidePalette.with(thumbnail)
//                        .use(Config.GLIDE_PALETTE_PROFILE)
//                        .intoBackground(ivThumb)
//                        .crossfade(true)
//                )
//                .into(ivThumb);
    }

    /**
     * Set text, load image article object
     */
    private void showArticle() {
        if (article == null)
            return;
        String thumbnail = article.getThumbnail();
        String title = article.getTitle();
        String date = DateHelper.parseDate(article.getPublish_date());
        String content = article.getContent();
        if (content == null)
            content = article.getExcerpt();
        String mosque = article.getMasjid();

        tvTitle.setText(title);
        tvDate.setText(date);
        tvMosque.setText(mosque);
        tvReadTotal.setVisibility(View.GONE);
        if (content != null)
            tvContent.setText(HtmlHelper.fromHtml(content));
        Glide.with(this)
                .load(article.getThumbnail())
                .listener(GlidePalette.with(thumbnail)
                        .use(Config.GLIDE_PALETTE_PROFILE)
                        .intoBackground(ivThumb)
                        .crossfade(true)
                )
                .into(ivThumb);
    }

    /**
     * Show error page
     */
    private void showError() {
        //Get content
        String content = "";
//        if (type.equals(Feed.TYPE_ARTICLE))
//            content = article.getContent();
//        else if (type.equals(Feed.TYPE_EVENT))
//            content = event.getDescription();
//        else if (type.equals(Feed.TYPE_INFAQ))
//            content = infaq.getDescription();
//        else if (type.equals(Feed.TYPE_MODULE))
//            content = module.getDescription();

        //Validate null object
        if (content == null)
            content = tvContent.getText().toString();
        if (vaMain != null) {
            if (content.equals(""))
                vaMain.setDisplayedChild(LAYOUT_FAILED);
            else if (!content.equals("")) {
                vaMain.setDisplayedChild(LAYOUT_MAIN);
                showToast(R.string.message_failed_fetch);
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }
}