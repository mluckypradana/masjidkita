package org.masjidkita.android.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import org.masjidkita.android.database.models.Article;
import org.masjidkita.android.helpers.SessionHelper;

import java.sql.SQLException;

/**
 * A login screen that offers login via email/password.
 */
public class SplashActivity extends CoreActivity {


    private int mUserId;
    private boolean mLoggedIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        setContentView(R.layout.activity_splash);
//        ButterKnife.bind(this);

        checkLoginSession();
        new DatabaseTask().execute();
    }

    /**
     * Start LoginActivity / MainActivitys
     */
    private void proceedToNextPage() {
        Intent intent = mLoggedIn ? new Intent(this, MainActivity.class) : new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition();
        finish();
    }

    /**
     * Check if apps has any user login
     */
    private void checkLoginSession() {
        mUserId = getSession().getInt(SessionHelper.USER_ID);
        if (mUserId != 0)
            mLoggedIn = true;
    }

    class DatabaseTask extends AsyncTask<String, String, Boolean> {

        @Override
        protected Boolean doInBackground(String... strings) {
            try {
                getDatabase().getDao(Article.class).countOf();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            proceedToNextPage();
        }
    }
}