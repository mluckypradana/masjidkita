package org.masjidkita.android.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ViewAnimator;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import org.masjidkita.android.Config;
import org.masjidkita.android.R;
import org.masjidkita.android.helpers.LayoutHelper;
import org.masjidkita.android.helpers.SessionHelper;
import org.masjidkita.android.network.response.BaseResponse;
import org.masjidkita.android.network.response.LoginResponse;
import org.masjidkita.android.network.response.RegisterResponse;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends CoreActivity implements
        GoogleApiClient.OnConnectionFailedListener,
        Validator.ValidationListener {
    //TODO forgot password

    @Email(messageResId = R.string.error_email_invalid)
    @Bind(R.id.et_email)
    EditText etEmail;
    @Bind(R.id.b_facebook_login)
    LoginButton bFacebookLogin;
    @NotEmpty(messageResId = R.string.error_password_empty)
    @Password(min = 6, messageResId = R.string.error_password_length)
    @Bind(R.id.et_password)
    EditText etPassword;

    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGN_IN = 9001;
    @Bind(R.id.va_main)
    ViewAnimator vaMain;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private GoogleApiClient mGoogleApiClient;
    private Validator mValidator;
    private String facebookAccessToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //Remove status bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

//        getSupportActionBar().hide();
        setContentView(R.layout.activity_login);
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        initValidator();
        initFacebookLoginButton();
        initGoogleLogin();
        initFirebaseAuth();

        checkRegisterMessage();
    }

    /**
     * Get message extra from intent
     */
    private void checkRegisterMessage() {
        String message = getIntent().getStringExtra(Config.Extra.MESSAGE);

        //Show dialog if message not null
        if (message != null) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage(message).show();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == REQUEST_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else {
                // Google Sign In failed, update UI appropriately
                // [START_EXCLUDE]
//                registerWithGoogleUser(null);
                // [END_EXCLUDE]
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
        Toast.makeText(this, "Google Play Services error.", Toast.LENGTH_SHORT).show();
    }

    /**
     * Validate text, on validated > login
     */
    @OnClick(R.id.b_login)
    void validateLogin() {
        LayoutHelper.hideKeyboard(this);

        //Validate, coninue to listeners
        mValidator.validate();
//        saveSession();
//        showDashboardPage();
    }

    @OnClick(R.id.b_google)
    void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, REQUEST_SIGN_IN);
    }

    @OnClick(R.id.b_facebook)
    void loginWithFacebook() {
//        saveSession();
//        showDashboardPage();
        bFacebookLogin.performClick();
    }

    /**
     * Start RegisterActivity
     */
    @OnClick(R.id.tv_register)
    void showRegisterPage() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
        overridePendingTransition();
    }

    /**
     * Call logn API
     */
    private void login() {
        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();

        showLoading();
        Call<LoginResponse> call = getService().login(email, password);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                LoginResponse body = response.body();
                if (response.code() == 200 && body.getStatus() == BaseResponse.STATUS_SUCCESS) {
                    saveSession(body);
                    showDashboardPage();
                } else
                    showError(body != null ? body.getMessage() : getResources().getString(R.string.error_login_with_email));
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                showError(getResources().getString(R.string.error_login_with_email));
            }
        });
    }

    private void showError(String message) {
        if (vaMain == null)
            return;
        vaMain.setDisplayedChild(LAYOUT_MAIN);
        showToast(message);
    }

    /**
     * Show progress while call login
     */
    private void showLoading() {
        vaMain.setDisplayedChild(LAYOUT_PROGRESS);
    }

    /**
     * Save credential to shared preferences
     */
    private void saveSession(LoginResponse response) {
        getSession().put(SessionHelper.USER_ID, response.getUser().getUserId());
        getSession().put(SessionHelper.TOKEN, response.getUser().getAccessToken());
    }


    private void initFacebookLoginButton() {
        CallbackManager mCallbackManager = CallbackManager.Factory.create();
        bFacebookLogin.setReadPermissions("email", "public_profile");
        bFacebookLogin.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                firebaseAuthWithFacebook(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
            }
        });
    }

    private void initFirebaseAuth() {
// [START initialize_auth]
        mAuth = FirebaseAuth.getInstance();
        // [END initialize_auth]

        // [START auth_state_listener]
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                    registerWithGoogleUser(user);
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // [START_EXCLUDE]
                // [END_EXCLUDE]
            }
        };
        // [END auth_state_listener]
    }

    private void initGoogleLogin() {
        //Don't forget to add SHAthis PC to your project settings
        // [START config_signin]
        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        // [END config_signin]

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    /**
     * Validator using saripaar
     */
    private void initValidator() {
        mValidator = new Validator(this);
        mValidator.setValidationListener(this);
    }

    /**
     * Get access token, change with firebase credential, authenticate with firebase credential
     *
     * @param token Access token from facebook
     */
    private void firebaseAuthWithFacebook(AccessToken token) {
        Log.d(TAG, "firebaseAuthWithFacebook:" + token);
        facebookAccessToken = token.getToken();
        AuthCredential credential = FacebookAuthProvider.getCredential(facebookAccessToken);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.

                        //Continue to firebase onAuthStateChanged if success
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCredential", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                });
    }

    /**
     * Change google credential to firebase credential
     *
     * @param acct
     */
    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());
        // [START_EXCLUDE silent]
//        showProgressDialog();
        // [END_EXCLUDE]

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        //Continue to auth listener
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCredential", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                        // [START_EXCLUDE]
//                        hideProgressDialog();
                        // [END_EXCLUDE]
                    }
                });
    }

    /**
     * Register user with google user account
     *
     * @param user
     */
    private void registerWithGoogleUser(FirebaseUser user) {
        showLoading();
        Call<RegisterResponse> call = getService().registerWithSocialAccount(
                null,
                null,
                null,
                null,
                null
        );
        call.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                showDashboardPage();
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                showDashboardPage();
            }
        });
    }

    /**
     * Start mainActivity
     */
    private void showDashboardPage() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition();
        finish();
    }

    @Override
    public void onValidationSucceeded() {
//        //Next validation
//        if (etPassword.getText().toString().length() >= MIN_PASSWORD_LENGTH)
        login();
//        else
//            etPassword.setError(getString(R.string.error_password_length));
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            //Show error to every editText
            EditText editText = (EditText) error.getView();
            editText.setError(error.getCollatedErrorMessage(this));
        }
    }
}