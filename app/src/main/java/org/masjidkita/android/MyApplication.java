package org.masjidkita.android;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.multidex.MultiDexApplication;
import android.util.Base64;
import android.util.Log;

import com.facebook.FacebookSdk;

import org.masjidkita.android.database.DatabaseHelper;
import org.masjidkita.android.helpers.SessionHelper;
import org.masjidkita.android.network.ApiService;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by MuhammadLucky on 09/08/2016.
 */
public class MyApplication extends MultiDexApplication {
    private SessionHelper session;
    private ApiService service;
    private DatabaseHelper database;

    @Override
    public void onCreate() {
        super.onCreate();

//        Cache cache = null;
//        try {
//            cache = new Cache(cacheFile, 10 * 1024 * 1024);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


        FacebookSdk.sdkInitialize(this);
        getFacebookHash();

        //Retrofit init
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor())
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        service = retrofit.create(ApiService.class);

        session = new SessionHelper(getApplicationContext());

        database = new DatabaseHelper(getApplicationContext());
//        //Do Realm things
//        RealmConfiguration.Builder realmConfig = new RealmConfiguration.Builder(this);
//        realmConfig.name(Config.OrmConfig.NAME);
//        realmConfig.schemaVersion(Config.OrmConfig.VERSION);
//        Realm.setDefaultConfiguration(realmConfig.build());
//        RealmController.with(this);
//        mRealm = RealmController.with(this).getDatabase();
    }

    private void getFacebookHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "org.masjidkita.android", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.i("KeyHash:",
                        Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

    public ApiService getService() {
        return service;
    }

    /**
     * Share preferences
     *
     * @return
     */
    public SessionHelper getSession() {
        return session;
    }

    public DatabaseHelper getDatabase() {
        return database;
    }

//    class ToStringConverterFactory extends Converter.Factory {
//        private final MediaType MEDIA_TYPE = MediaType.parse("text/plain");
//
//        @Override
//        public Converter<ResponseBody, ?> fromResponseBody(Type type, Annotation[] annotations) {
//            if (String.class.equals(type)) {
//                return new Converter<ResponseBody, String>() {
//                    @Override public String convert(ResponseBody value) throws IOException {
//                        return value.string();
//                    }
//                };
//            }
//            return null;
//        }
//
//        @Override public Converter<?, RequestBody> toRequestBody(Type type, Annotation[] annotations) {
//            if (String.class.equals(type)) {
//                return new Converter<String, RequestBody>() {
//                    @Override public RequestBody convert(String value) throws IOException {
//                        return RequestBody.create(MEDIA_TYPE, value);
//                    }
//                };
//            }
//            return null;
//        }
//    }
}
