package org.masjidkita.android.holder;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.masjidkita.android.R;
import org.masjidkita.android.listeners.OnItemClickListener;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by MuhammadLucky on 02/09/2016.
 */
public class FeedHolder extends RecyclerView.ViewHolder {
    OnItemClickListener listener;

    @Bind(R.id.iv_thumb)
    public ImageView ivThumb;
    @Bind(R.id.tv_mosque)
    public TextView tvMosque;
    @Bind(R.id.tv_date)
    public TextView tvDate;
    @Bind(R.id.tv_title)
    public TextView tvTitle;
    @Bind(R.id.tv_content)
    public TextView tvContent;
    @Bind(R.id.cv_main)
    public CardView cvMain;
    @Bind(R.id.ll_action)
    public LinearLayout llAction;
    @Bind(R.id.tv_label_1)
    public TextView tvLabel1;
    @Bind(R.id.tv_detail_1)
    public TextView tvDetail1;
    @Bind(R.id.tv_label_2)
    public TextView tvLabel2;
    @Bind(R.id.tv_detail_2)
    public TextView tvDetail2;
    @Bind(R.id.ll_detail)
    public LinearLayout llDetail;

    FeedHolder(OnItemClickListener listener, View view) {
        super(view);
        this.listener = listener;
        ButterKnife.bind(this, view);
    }

    @OnClick(R.id.cv_main)
    void showDetail() {
        if (listener != null)
            listener.onItemClicked(getAdapterPosition());
    }
}