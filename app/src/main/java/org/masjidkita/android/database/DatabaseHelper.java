package org.masjidkita.android.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import org.masjidkita.android.Config;
import org.masjidkita.android.database.models.Article;
import org.masjidkita.android.database.models.Event;
import org.masjidkita.android.database.models.Feed;
import org.masjidkita.android.database.models.FeedDetail;
import org.masjidkita.android.database.models.Infaq;
import org.masjidkita.android.database.models.Module;
import org.masjidkita.android.database.models.Mosque;
import org.masjidkita.android.database.models.User;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private final Context context;
    private ArrayList<Class> classes = new ArrayList<>();

    //Change this variable

    public DatabaseHelper(Context context) {
        super(context, Config.OrmConfig.NAME, null, Config.OrmConfig.VERSION);
        this.context = context;

        //Add new class here for table
        classes = new ArrayList<>();
        classes.add(Article.class);
        classes.add(Event.class);
        classes.add(Feed.class);
        classes.add(FeedDetail.class);
        classes.add(Infaq.class);
        classes.add(Module.class);
        classes.add(Mosque.class);
        classes.add(User.class);
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        createAllTable(connectionSource);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource,
                          int oldVersion, int newVersion) {
        resetDatabase();
    }

    public void resetDatabase() {
        dropAllTable(getConnectionSource());
        createAllTable(getConnectionSource());
    }

    private void createAllTable(ConnectionSource connectionSource) {
//        List<String> classess = ClassHelper.getClasses(context,
//                Config.OrmConfig.MODEL_PACKAGE);
        for (Class c : classes) {
//            Class c = null;
//            try {
//                c = Class.forName(strClass);
//            } catch (ClassNotFoundException e) {
//                e.printStackTrace();
//            }
            if (c != null) {
                try {
                    TableUtils.createTableIfNotExists(connectionSource, c);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void dropAllTable(ConnectionSource connectionSource) {
//        List<String> classess = ClassHelper.getClasses(context,
//                Config.OrmConfig.MODEL_PACKAGE);
        for (Class c : classes) {
//            Class c = null;
//            try {
//                c = Class.forName(strClass);
//            } catch (ClassNotFoundException e) {
//                e.printStackTrace();
//            }
            if (c != null) {
                try {
                    TableUtils.dropTable(connectionSource, c, true);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    @Override
    public void close() {
        super.close();
    }

}