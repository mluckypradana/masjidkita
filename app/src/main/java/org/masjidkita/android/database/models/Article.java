package org.masjidkita.android.database.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by MuhammadLucky
 * Realm object for database, for "article" object in API
 */
@DatabaseTable
public class Article extends CoreModel {
    //Field names
    public static String DATE = "publish_date";
    public static String FROM_FOLLOWED_MOSQUE = "from_followed_mosque";

    //    @PrimaryKey
    @DatabaseField(id = true)
    private Integer id;
    @DatabaseField
    private String category;
    @DatabaseField
    private String title;
    @DatabaseField
    private String excerpt;
    @DatabaseField
    private String content;
    @DatabaseField
    private String masjid;
    @DatabaseField
    private String user_id;
    @DatabaseField
    private String thumbnail;
    @DatabaseField
    private String publish_date;
    //For menu article
    @DatabaseField
    private boolean from_followed_mosque = false;

    public Article() {
    }

    /**
     * Create article from feed
     *
     * @param feed
     */
    public Article(Feed feed) {
        id = feed.getId();
        category = feed.getType();
        title = feed.getName();
        excerpt = feed.getExcerpt();
        masjid = feed.getMasjid();
        thumbnail = feed.getThumbnail();
        publish_date = feed.getDate();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getExcerpt() {
        return excerpt;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMasjid() {
        return masjid;
    }

    public void setMasjid(String masjid) {
        this.masjid = masjid;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getPublish_date() {
        return publish_date;
    }

    public void setPublish_date(String publish_date) {
        this.publish_date = publish_date;
    }

    public boolean isFrom_followed_mosque() {
        return from_followed_mosque;
    }

    public void setFrom_followed_mosque(boolean from_followed_mosque) {
        this.from_followed_mosque = from_followed_mosque;
    }
}
