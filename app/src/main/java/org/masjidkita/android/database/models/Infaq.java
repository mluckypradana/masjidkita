package org.masjidkita.android.database.models;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by MuhammadLucky
 * Realm object for database, for "article" object in API
 */
public class Infaq extends CoreModel {//extends RealmObject {
    public static String STATUS_EXPIRED = "expired";
    public static String FROM_FOLLOWED_MOSQUE = "from_followed_mosque";

    //Field names
    public static String DATE = "date";

    @DatabaseField(id = true)
    private Integer id;
    @DatabaseField
    private String name;
    @DatabaseField
    private String masjid;
    @DatabaseField
    private long target;
    //As current collected infaq?
    @DatabaseField
    private long nominal;
    @DatabaseField
    private String description;
    @DatabaseField
    private String date;
    @DatabaseField
    private String expired_date;
    @DatabaseField
    private String thumbnail;
    @DatabaseField
    private String status;
    @DatabaseField
    private String excerpt;

    //Fields for my infaq
    @DatabaseField
    private Integer infaq_id;
    @DatabaseField
    private Integer infaq_name;
    @DatabaseField
    private long income;
    @DatabaseField
    private boolean from_followed_mosque = false;
    //Also date, expired_date, and status


    public Infaq() {
    }

    public Infaq(Feed feed) {
        id = feed.getId();
        name = feed.getName();
        excerpt = feed.getExcerpt();
        masjid = feed.getMasjid();
        thumbnail = feed.getThumbnail();
        date = feed.getDate();
        if (feed.getFeedDetail() != null)
            target = feed.getFeedDetail().getTarget();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMasjid() {
        return masjid;
    }

    public void setMasjid(String masjid) {
        this.masjid = masjid;
    }

    public long getTarget() {
        return target;
    }

    public void setTarget(long target) {
        this.target = target;
    }

    public long getNominal() {
        return nominal;
    }

    public void setNominal(long nominal) {
        this.nominal = nominal;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getExpired_date() {
        return expired_date;
    }

    public void setExpired_date(String expired_date) {
        this.expired_date = expired_date;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getExcerpt() {
        if (excerpt == null)
            excerpt = "";
        return excerpt;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }
}
