package org.masjidkita.android.database.models;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by MuhammadLucky
 */
@DatabaseTable
public class Feed extends CoreModel {
    public static String TYPE_FEED = "article";
    public static String TYPE_ARTICLE = "article";
    public static String TYPE_EVENT = "event";
    public static String TYPE_INFAQ = "infaq";
    public static String TYPE_MODULE = "module";

    //Field names
    public static String DATE = "date";

    @DatabaseField(id = true)
    private Integer id;
    @DatabaseField
    private String type;
    @DatabaseField
    private String name;
    @DatabaseField
    private String masjid;
    @DatabaseField
    private String thumbnail;
    @DatabaseField
    private String date;
    @DatabaseField
    private String excerpt;
    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    @SerializedName("lain-lain")
    private FeedDetail feedDetail;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMasjid() {
        return masjid;
    }

    public void setMasjid(String masjid) {
        this.masjid = masjid;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getExcerpt() {
        return excerpt;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }

    public FeedDetail getFeedDetail() {
        return feedDetail;
    }

    public void setFeedDetail(FeedDetail feedDetail) {
        this.feedDetail = feedDetail;
    }
}
