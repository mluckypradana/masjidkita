package org.masjidkita.android.database.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by MuhammadLucky on 21/08/2016.
 */
@DatabaseTable
public class EventDetail extends CoreModel {
    @DatabaseField(generatedId = true)
    private Integer id;
    @DatabaseField
    private String date;
    @DatabaseField
    private String timezone_type;
    @DatabaseField
    private String timezone;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTimezone_type() {
        return timezone_type;
    }

    public void setTimezone_type(String timezone_type) {
        this.timezone_type = timezone_type;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }
}
