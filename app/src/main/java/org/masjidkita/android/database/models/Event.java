package org.masjidkita.android.database.models;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by MuhammadLucky
 * As "activities" object
 */
@DatabaseTable
public class Event extends CoreModel {// extends RealmObject {
    //Field names
    public static String DATE = "date";
    public static String FROM_FOLLOWED_MOSQUE = "from_followed_mosque";

    @DatabaseField(id = true)
    private Integer id;
    @DatabaseField
    private String name;
    @DatabaseField
    private String masjid;
    @DatabaseField
    private String ustadz;
    @DatabaseField
    private String excerpt;
    @DatabaseField
    private String description;
    @DatabaseField
    private String date;
    @DatabaseField
    private String time;
    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    @SerializedName("created")
    private EventDetail eventDetail;
    //For menu article
    @DatabaseField
    private boolean from_followed_mosque = false;

    public Event() {
    }

    public Event(Feed feed) {
        id = feed.getId();
        name = feed.getName();
        excerpt = feed.getExcerpt();
        masjid = feed.getMasjid();
        date = feed.getDate();
        if (feed.getFeedDetail() != null)
            time = feed.getFeedDetail().getTime();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMasjid() {
        return masjid;
    }

    public void setMasjid(String masjid) {
        this.masjid = masjid;
    }

    public String getUstadz() {
        return ustadz;
    }

    public void setUstadz(String ustadz) {
        this.ustadz = ustadz;
    }

    public String getExcerpt() {
        return excerpt;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public EventDetail getEventDetail() {
        return eventDetail;
    }

    public void setEventDetail(EventDetail eventDetail) {
        this.eventDetail = eventDetail;
    }
}
