package org.masjidkita.android.database.models;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by MuhammadLucky
 * Realm object for database, for "module" object in API
 */
public class Module extends CoreModel {
    public static String TYPE_REQUEST_DOWNLOAD = "download";
    public static String TYPE_REQUEST_UPLOAD = "upload";
    public static String TYPE = "type";

    public static int TYPE_FEED = 0;
    public static int TYPE_DOWNLOAD = 1;
    public static int TYPE_UPLOAD = 2;

    @DatabaseField(id = true)
    private Integer id;
    @DatabaseField
    private String category;
    @DatabaseField
    private String user;
    @DatabaseField
    private String title;
    @DatabaseField
    private String description;
    @DatabaseField
    private String thumbnail;
    @DatabaseField
    private Integer hit = 0;

    //For uploaded modules
    @DatabaseField
    private String file;
    @DatabaseField
    private Integer downloaded = 0;
    @DatabaseField
    private Integer type = TYPE_FEED;

    /**
     * Ormlite need no argument constructor
     */
    public Module() {
    }

    public Module(Feed feed) {
        id = feed.getId();
        category = "";
        title = feed.getName();
        description = feed.getExcerpt();
        thumbnail = feed.getThumbnail();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Integer getHit() {
        return hit;
    }

    public void setHit(Integer hit) {
        this.hit = hit;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getDownloaded() {
        return downloaded;
    }

    public void setDownloaded(Integer downloaded) {
        this.downloaded = downloaded;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
}
