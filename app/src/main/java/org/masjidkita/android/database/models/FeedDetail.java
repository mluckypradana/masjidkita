package org.masjidkita.android.database.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by MuhammadLucky on 21/08/2016.
 */
@DatabaseTable
public class FeedDetail extends CoreModel {
    public static String ID = "id";
    public static String FEED_ID = "feed_id";

    @DatabaseField(generatedId = true)
    private Integer id;
    @DatabaseField
    private long target = 0;
    @DatabaseField
    private String time;
    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    private Feed feed;

    public long getTarget() {
        return target;
    }

    public void setTarget(long target) {
        this.target = target;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Feed getFeed() {
        return feed;
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
    }
}
