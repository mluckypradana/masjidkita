##Start Date
August, 8th 2016

## Android Minimum Version	
API Level 15

## System dependencies
* [Firebase](https://firebase.google.com/docs/auth/android/facebook-login) integrate with Email login, Facebook login, Google login.
* [Facebook SDK](https://developers.facebook.com/docs/android/getting-started).
* [Retrofit](http://square.github.io/retrofit/) (Web Service Framework), documentation [here](http://inthecheesefactory.com/blog/retrofit-2.0/en).
* [Butter Knife](http://jakewharton.github.io/butterknife/) (View Injection).
* [Butter Knife Zelezny](https://github.com/avast/android-butterknife-zelezny) (View Injection Generator).
* Glide Image Loader.
* [Glide Pallete](https://github.com/florent37/GlidePalette).
* Glide Transformation.

##Facebook setting for Firebase
* Link [here](https://firebase.google.com/docs/auth/android/manage-users).

##Google setting for Firebase
* Link [here](http://stackoverflow.com/questions/34324258/google-sign-in-not-working-android)